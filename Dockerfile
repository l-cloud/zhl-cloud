FROM moxm/java:1.8-full

MAINTAINER cqgszhouhailong@163.com

# 设置工作目录，进入到容器中的初始目录,不存在会自动创建
ENV MYPATH /root/springboot

ENV TZ=Asia/Shanghai JAVA_OPTS="-Xms128m -Xmx256m -Djava.security.egd=file:/dev/./urandom"

RUN ln -sf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# RUN mkdir -p /export-boot
# 设置工作目录，进入到容器中的初始目录,不存在会自动创建
WORKDIR $MYPATH
# 暴露端口
EXPOSE 9999
# 复制jar到工作目录并改名为app.jar
# 相对于Dockerfile文件的路径
# ADD 命令必须提供两个参数，第一个参数为：宿主机中的目录，相对于Dockerfile文件
# 第二个参数为容器中的目录，相对于WORKDIR
ADD ./target/export-boot.jar app.jar
# 添加容器卷，方便以后的处理，关联宿主机中的目录，不存在会自动创建
VOLUME $MYPATH
#   执行 java -jar 命令 （CMD：在启动容器时才执行此行。RUN：构建镜像时就此行，后面的jar包路径就是上面要设置的jar包路径）
CMD sleep 60;java $JAVA_OPTS -jar app.jar
