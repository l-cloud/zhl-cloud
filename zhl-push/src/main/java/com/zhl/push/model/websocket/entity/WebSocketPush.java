package com.zhl.push.model.websocket.entity;

import com.zhl.common.vo.Result;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * @Description: websocket推送
 * @version： V1.0
 */
@Data
public class WebSocketPush implements Serializable {
    private static final long serialVersionUID = 1L;
	/**消息推送模块*/
	@NotNull(message = "消息推送模块不能为空")
	private String model;
	/**推送ID类型（1：部门  2：用户）*/
	private Integer pushType;
	/**存储推送到前端的flag等所需信息*/
	private Result result;
	/**ID集合 部门ID 或 userId*/
	private List<String> idsList;


}
