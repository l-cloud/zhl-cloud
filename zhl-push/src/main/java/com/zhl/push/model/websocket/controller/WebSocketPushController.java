package com.zhl.push.model.websocket.controller;

import com.zhl.common.vo.Result;
import com.zhl.push.model.websocket.entity.WebSocketPush;
import com.zhl.push.util.WebSocketUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;


/**
* @Title: Controller
* @Description: websocket消息推送接口
*/
@Slf4j
@RestController
@RequestMapping("api/websocket")
public class WebSocketPushController {

    @Autowired
    private WebSocketUtil webSocketUtils;


    /**
     * websocket消息推送
     * @param webSocketPush
     * @return
     */
    @PostMapping(value = "/websocketPush")
    public Result notifyPush(@RequestBody@Valid WebSocketPush webSocketPush) {
        Result result = webSocketUtils.websocketPushCommon(webSocketPush);
        return result;
    }


}
