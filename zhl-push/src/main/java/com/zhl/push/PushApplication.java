package com.zhl.push;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author zhl
 * @Title: PushApplication
 * @Description TODO
 * @date： 2020/10/14 16:16
 * @version： V1.0
 */
@SpringBootApplication(scanBasePackages = "com.zhl")
@EnableDiscoveryClient
@Slf4j
public class PushApplication {
    public static void main(String[] args) {
        SpringApplication.run(PushApplication.class);
    }
}
