package com.zhl.push.config;


import com.zhl.common.vo.Result;
import net.sf.json.JSONObject;

import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;

/**
 * 为了在websocket服务器使用sendObject给客户端发送object,需要实现如下ServerEncoder类
 * ServerEncoder为了防止在websocket向客户端发送sendObject方法的时候提示如下错误：
 * javax.websocket.EncodeException: No encoder specified for object of class [class org.ywzn.po.Messagepojo]
 * @author 凌晨
 * @Title: ServerEncoder
 * @Description TODO
 * @date： 2019/7/25 10:06
 * @version： V1.0
 */
public class ServerEncoder implements Encoder.Text<Result> {

    /**
     * 代表websocket调用sendObject方法返回客户端的时候，必须返回的是JSONObject对象
     * 在encode()的这个方法内，可以自定义要返回的值，这里我需要的是Java对象转成的Json格式字符串
     * @param result
     * @return
     * @throws EncodeException
     */
    @Override
    public String encode(Result result) throws EncodeException {
        //将java对象转换为json字符串
        return JSONObject.fromObject(result).toString();
    }

    @Override
    public void init(EndpointConfig endpointConfig) {

    }

    @Override
    public void destroy() {

    }

}

