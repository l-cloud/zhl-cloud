package com.zhl.monitor.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 凌晨
 * @Title: TestController
 * @Description TODO
 * @date： 2021/6/24 19:00
 * @version： V1.0
 */
@RestController
public class TestController {

    @GetMapping("/test")
    public String test(@RequestParam String param) throws InterruptedException {
        Thread.sleep(2000);
        return param;
    }

}
