package com.zhl.message;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

/**
 * @author 凌晨
 * @Title: MsgApplication
 * @Description TODO
 * @date： 2020/10/14 19:15
 * @version： V1.0
 */
@SpringBootApplication(scanBasePackages = "com.zhl")
@EnableDiscoveryClient
@Slf4j
public class MsgApplication {
    public static void main(String[] args) {
        ConfigurableApplicationContext application = SpringApplication.run(MsgApplication.class,args);
        Environment env = application.getEnvironment();
        String port = env.getProperty("server.port");
        String path = env.getProperty("server.servlet.context-path");
        String server = env.getProperty("spring.cloud.stream.rocketmq.binder.name-server");
        String input1 = env.getProperty("spring.cloud.stream.bindings.input1.group");
        String input2 = env.getProperty("spring.cloud.stream.bindings.input2.group");
        String input3 = env.getProperty("spring.cloud.stream.bindings.input3.group");
        String input4 = env.getProperty("spring.cloud.stream.bindings.input4.group");
        log.info("spring.cloud.stream.rocketmq.binder.name-server:"+server+"");
        log.info("spring.cloud.stream.bindings.input1.group:"+input1+"");
        log.info("spring.cloud.stream.bindings.input2.group:"+input2+"");
        log.info("spring.cloud.stream.bindings.input3.group:"+input3+"");
        log.info("spring.cloud.stream.bindings.input4.group:"+input4+"");
    }
}
