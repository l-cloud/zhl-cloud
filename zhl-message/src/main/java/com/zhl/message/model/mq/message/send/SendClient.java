package com.zhl.message.model.mq.message.send;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.stereotype.Component;

/**
 * @author zhl
 * @Title: SendClient
 * @Description TODO
 * @date： 2020/8/3 10:54
 * @version： V1.0
 */
@Component
public class SendClient {

    @Autowired
    private MySource source;

    public void send(String msg) throws Exception {
        source.output1().send(MessageBuilder.withPayload(msg).build());
    }
}
