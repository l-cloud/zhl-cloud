package com.zhl.message.model.mq.message.send;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

/**
 * @author zhl
 * @Title: MySource
 * @Description TODO
 * @date： 2020/8/3 10:53
 * @version： V1.0
 */
public interface MySource {

    String OUTPUT1 = "output1";
    String OUTPUT2 = "output2";

    @Output(MySource.OUTPUT1)
    MessageChannel output1();

    @Output(MySource.OUTPUT2)
    MessageChannel output2();
}
