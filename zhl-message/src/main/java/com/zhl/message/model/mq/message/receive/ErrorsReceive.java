package com.zhl.message.model.mq.message.receive;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.support.ErrorMessage;
import org.springframework.stereotype.Component;

/**
 * @author zhl
 * @Title: ErrorsReceive
 * @Description 在接收消息时，如果消息处理失败，
 * Spring Cloud会把失败的消息转到名为<destination>.<group>.errors的Channel，
 * 并可通过@ServiceActivator方法进行接收。比如有如下Binding定义。
 * @date： 2020/8/3 13:38
 * @version： V1.0
 */
@Component
@Slf4j
public class ErrorsReceive {

    /**
     * 处理某个特定Binding的消息消费异常的
     * @param message
     */
    @ServiceActivator(inputChannel = "test-topic1.test-group1.errors")
    public void handleConsumeUserError(ErrorMessage message) {
        log.info("收到处理失败的消息{}", message.getPayload());
    }

    /**
     * 一个统一的处理所有的消息消费异常的入口
     * 可以定义一个Binding名为errorChannel的@StreamListener方法
     * @param message
     */
    @StreamListener("errorChannel")
    public void handleErrors(ErrorMessage message) {
        log.info("默认的消息失败处理器收到处理失败的消息: {}，headers：{}", message.getOriginalMessage(), message.getHeaders());
    }
}
