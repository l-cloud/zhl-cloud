package com.zhl.message.model.mq.message.receive;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

/**
 * @author zhl
 * @Title: MySink
 * @Description 自定义 Input 接口
 * @date： 2020/8/3 10:50
 * @version： V1.0
 */
public interface MySink {

    String INPUT1 = "input1";
    String INPUT2 = "input2";
    String INPUT3 = "input3";
    String INPUT4 = "input4";

    /**
     * 当不通过@Input指定Binding的名称时，默认会使用方法名作为Binding的名称。
     * @return
     */
    @Input(MySink.INPUT1)
    SubscribableChannel input1();

    @Input(MySink.INPUT2)
    SubscribableChannel input2();

    @Input(MySink.INPUT3)
    SubscribableChannel input3();

    @Input(MySink.INPUT4)
    SubscribableChannel input4();
}
