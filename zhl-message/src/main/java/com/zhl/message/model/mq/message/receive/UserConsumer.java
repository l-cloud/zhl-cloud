package com.zhl.message.model.mq.message.receive;

import com.zhl.common.vo.Result;
import com.zhl.message.model.mq.message.send.MySource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;

/**
 * @author zhl
 * @Title: UserConsumer
 * @Description TODO
 * @date： 2020/8/3 13:35
 * @version： V1.0
 */
@Slf4j
@Component
public class UserConsumer {

    @Autowired
    MySource streamClient;

    /**
     * 接收的是String类型的，你会发现接收到的字符串是JSON格式的，
     * 因为发送端默认会把对象转换为JSON格式进行发送
     * @param user
     */
//    @StreamListener(value = MySink.INPUT1,condition = "headers['flag']=='aa'")
    @StreamListener(value = MySink.INPUT1) //Cannot set a condition for methods that return a value
//    边接收边发送 也可以通过@Output指定返回的内容将发送到的Binding的名称。
    @SendTo(MySource.OUTPUT2)
//    An output channel cannot be specified for a method that does not return a value
    public Result consumeUser(String user) {
        log.info("从Binding-{}收到String类型的消息-{}", MySink.INPUT1, user);
//        streamClient.output2().send(MessageBuilder.withPayload(Result.ok("我收到了发送的消息")).build());
        return Result.ok("我收到了发送的消息");
    }

    /**
     * 这里也可以直接以Result类型进行接收，
     * 此时Spring Cloud将自动将接收到的JSON字符串转换为消费者方法的入参对象
     * @param result
     */
//    @StreamListener(MySink.INPUT1)
//    public void consumeUser(Result result) {
//        log.info("从Binding-{}收到Result类型的消息-{}", MySink.INPUT1, result);
//    }

    @StreamListener(MySink.INPUT2)
    public void consumeUser2(Result result) {
//        log.info("回调消息", MySink.INPUT2, result);
        log.info("回调消息,"+result+"");
    }

}
