package com.zhl.message.model.mq.controller;

import cn.hutool.core.date.DateUtil;
import com.zhl.common.vo.Result;
import com.zhl.message.model.mq.message.send.MySource;
import com.zhl.model.sys.user.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author zhl
 * @Title: MQRestController
 * @Description TODO
 * @date： 2020/8/3 13:55
 * @version： V1.0
 */
@RestController
@Slf4j
public class MQRestController {

    @Autowired
    MySource streamClient;

    @GetMapping("mq")
    public void test(){
        log.info("***************开始发送mq消息***************");
        String now = "now" + new Date();
        streamClient.output1().send(MessageBuilder.withPayload(now).build());
        log.info("***************结束发送mq消息***************");
    }

    @GetMapping("mqUser")
    public Result sendUser() {
        log.info("***************开始发送mq消息***************");
        User user1 = new User();
        user1.setId(1000L)
                .setCreateTime(new Date())
                .setUsername("测试姓名1")
                .setAge(28).setAddress("北京市海淀区春园10号路111")
                .setBirthday(DateUtil.parse("19911203"))
                .setEmail("123@qq.com")
                .setMobile("15808314391")
                .setSex(1)
                .setIdCard("500230199112042234")
                .setStatus(1).setQq("123456789").setWx("66666");
        User user2 = new User();
        user2.setId(1000L)
                .setCreateTime(new Date())
                .setUsername("测试姓名2")
                .setAge(28).setAddress("北京市丰台区春园10号路111")
                .setBirthday(DateUtil.parse("19911203"))
                .setEmail("123@qq.com")
                .setMobile("13608314391")
                .setSex(1)
                .setIdCard("500230199112042234")
                .setStatus(1).setQq("123456789").setWx("66666");
//        Map<String, Object> headers = new HashMap<String, Object>();
//        headers.put(MessageConst.PROPERTY_TAGS, "userTag");
//        headers.put("flag","aa");
//        MessageHeaders messageHeaders = new MessageHeaders(headers);
//        Message<User> message = MessageBuilder.createMessage(user, messageHeaders);

        List<User> userList = new ArrayList<>();
        userList.add(user1);
        userList.add(user2);
        Result<List<User>> result = new Result<>();
        result.setResult(userList);
//        this.streamClient.output1().send(message);
        this.streamClient.output1()
                .send(MessageBuilder.withPayload(result)
                .setHeader("flag","bb")
                .build());
        log.info("***************结束发送mq消息***************");
        return Result.ok("发送rocketmq消息成功,发送目的地："+streamClient.output1()+"");
    }
}
