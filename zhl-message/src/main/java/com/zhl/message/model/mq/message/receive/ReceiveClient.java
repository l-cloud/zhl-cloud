package com.zhl.message.model.mq.message.receive;

import org.springframework.stereotype.Component;

/**
 * @author zhl
 * @Title: ReceiveClient
 * @Description TODO
 * @date： 2020/8/3 10:46
 * @version： V1.0
 */
@Component
public class ReceiveClient {

    // If multiple @StreamListener methods are listening to the same binding target,
    // none of them may return a value
//    @StreamListener(value = MySink.INPUT1,condition = "headers['flag']=='bb'")
//    public void receiveInput1(String receiveMsg) {
//        System.out.println("input1 receive: " + receiveMsg);
//    }
}
