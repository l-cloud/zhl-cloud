# 1.构建步骤

    上传安装包，首先我们需要把要构建的软件安装包上传到服务器中，我们可以在服务器目录上创建一个专门的文件夹，如：/var/nginx_build
    编写一个dockerfile文件
    通过docker build构建文件的一个镜像
    docker run 运行镜像
    docker push 发布镜像
    
# 2.dockerfile文件编写基本步骤和注意事项

    每个保留关键字必须大写
    执行顺序由上到下
    再文件中用“#”表示注释
    每一个指令都会去创建一个新的镜像层，并提交    
    
# 3.常用的Dockerfile指令

    注：常用指令引用了大神的，链接为：https://www.cnblogs.com/linjiqin/p/8735230.html
    
    1.FROM:格式为FROM image或FROM image:tag，并且Dockerfile中第一条指令必须是FROM指令，且在同一个Dockerfile中创建多个镜像时，可以使用多个FROM指令。
    2. MAINTAINER:格式为MAINTAINER user_name user_email，用于声明镜像是谁写的一般写姓名+邮箱
    
    3.RUN
    格式为RUN command或 RUN [“EXECUTABLE”,“PARAM1”,“PARAM2”…]，前者在shell终端中运行命令，/bin/sh -c command，例如：/bin/sh -c “echo hello”；后者使用exec执行，指定其他运行终端使用RUN["/bin/bash","-c",“echo hello”]每条RUN指令将当前的镜像基础上执行指令，并提交为新的镜像，命令较长的时候可以使用\来换行。
    
    4.CMD
    支持三种格式：
    CMD [“executable”,“param1”,“param2”]，使用exec执行，这是推荐的方式。
    CMD command param1 param2 在/bin/sh中执行。
    CMD [“param1”,“param2”] 提供给ENTERYPOINT的默认参数。
    CMD用于指定容器启动时执行的命令，每个Dockerfile只能有一个CMD命令，多个CMD命令只执行最后一个。若容器启动时指定了运行的命令，则会覆盖掉CMD中指定的命令。
    
    5.EXPOSE
    格式为 EXPOSE port [port2,port3,…]，例如EXPOSE 80这条指令告诉Docker服务器暴露80端口，供容器外部连接使用。
    在启动容器的使用使用-P，Docker会自动分配一个端口和转发指定的端口，使用-p可以具体指定使用哪个本地的端口来映射对外开放的端口。
    
    6.ENV
    格式为：EVN key value 。用于指定环境变量，这些环境变量，后续可以被RUN指令使用，容器运行起来之后，也可以在容器中获取这些环境变量。
    例如
    ENV word hello
    RUN echo $word
    
    7.ADD
    格式：ADD src dest
    该命令将复制指定本地目录中的文件到容器中的dest中，src可以是是一个绝对路径，也可以是一个URL或一个tar文件，tar文件会自动解压为目录。
    
    8.COPY
    格式为：COPY src desc
    复制本地主机src目录或文件到容器的desc目录，desc不存在时会自动创建。
    
    9.ENTRYPOINT
    格式有两种：
    ENTRYPOINT [“executable”,“param1”,“param2”]
    ENTRYPOINT command param1,param2 会在shell中执行。
    用于配置容器启动后执行的命令，这些命令不能被docker run提供的参数覆盖。和CMD一样，每个Dockerfile中只能有一个ENTRYPOINT，当有多个时最后一个生效。
    
    10.VOLUME
    格式为 VOLUME ["/data"]
    作用是创建在本地主机或其他容器可以挂载的数据卷，用来存放数据。
    
    11.USER
    格式为：USER username
    指定容器运行时的用户名或UID，后续的RUN也会使用指定的用户。要临时使用管理员权限可以使用sudo。在USER命令之前可以使用RUN命令创建需要的用户。
    例如：RUN groupadd -r docker && useradd -r -g docker docker
    
    12.WORKDIR
    格式： WORKDIR /path
    为后续的RUN CMD ENTRYPOINT指定配置工作目录，可以使用多个WORKDIR指令，若后续指令用得是相对路径，则会基于之前的命令指定路径。
    
    13.ONBUILD
    格式ONBUILD [INSTRUCTION]
    该配置指定当所创建的镜像作为其他新建镜像的基础镜像时所执行的指令。
    例如下面的Dockerfile创建了镜像A：
    ONBUILD ADD . /app
    ONBUILD RUN python app.py
    
    则基于镜像A创建新的镜像时，新的Dockerfile中使用from A 指定基镜像时，会自动执行ONBBUILD指令内容，等价于在新的要构建镜像的Dockerfile中增加了两条指令：
    FROM A
    ADD ./app
    RUN python app.py
    
# 3.一个Dockerfile文件案例

    FROM centos      #基于centos镜像进行增加
    MAINTAINER xiec<187xxx0191@qq.com>   #我的姓名和邮箱
    COPY readme.txt /usr/local/readme.txt   #将本地的readme.txt拷贝到容器中
    ADD jdk-8u131-linux-x64.tar.gz /usr/local  #将本地的jdk安装包拷贝到对应容器目录中
    ADD apache-tomcat-8.5.47.tar.gz /usr/local  #将本地的tomcat安装包拷贝到容器目录中
    RUN yum -y install vim   #再容器中执行这个命令安装vim指令
    ENV MYPATH /usr/local   #配置环境变量
    WORKDIR $MYPATH  # 设置镜像进去的工作路径
    ENV JAVA_HOME /usr/local/jdk1.8.0_131   #配置jdk和tomcat的环境变量
    ENV CLASSPATH $JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar
    ENV CATALINA_HOME /usr/local/apache-tomcat-8.5.47
    ENV CATALINA_BASH /usr/local/apache-tomcat-8.5.47
    ENV PATH $PATH:$JAVA_HOME/bin:$CATALINA_HOME/lib:$CATALINA_HOME/bin
    
    EXPOSE 8080  #容器对外暴漏的端口
    
    CMD /usr/local/apache-tomcat-8.5.47/bin/startup.sh && tail -F /url/local/apache-tomcat-8.5.47/bin/logs/catalina.out   # 再run运行这个容器的时候会运行的命令，用于启动tomcat
    
# 4.通过docker build命令就可以构建镜像

    docker build [OPTIONS] PATH | URL | -
    OPTIONS有很多指令，下面列举几个常用的：
    
    --build-arg=[] :设置镜像创建时的变量；
    -f :指定要使用的Dockerfile路径；
    --force-rm :设置镜像过程中删除中间容器；
    --rm :设置镜像成功后删除中间容器；
    --tag, -t: 镜像的名字及标签，通常 name:tag 或者 name 格式；
    
    当Dockerfile和当前执行命令的目录不在同一个时，我们也可以指定Dockerfile
    docker build -f dockerfile的文件名 -t 镜像名称：镜像版本（不指定为最新）
    构建完成后，通过docker run命令运行即可测试    
    
    例子：
    docker build -t second:v1.0 .
    镜像名称不能有大写
    注意最后有个点，代表使用当前路径的 Dockerfile 进行构建 ，
    -t  second : v1.0  给新构建的镜像取名为 second， 并设定版本为 v1.0 。
    -f 显示指定构建镜像的 Dockerfile 文件（Dockerfile 可不在当前路径下），
    如果不使用 -f，则默认将上下文路径下的名为 Dockerfile 的文件认为是构建镜像的 "Dockerfile" 。 
    上下文路径|URL： 指定构建镜像的上下文的路径，构建镜像的过程中，可以且只可以引用上下文中的任何文件 。 
    
# 可以用docker images 查看是否构建成功

# 进入容器命令：

    docker exec ：在运行的容器中执行命令
    
    docker exec [OPTIONS] CONTAINER COMMAND [ARG...]
    OPTIONS说明：
    
    -d :分离模式: 在后台运行
    
    -i :即使没有附加也保持STDIN 打开
    
    -t :分配一个伪终端

    1、docker attach 容器ID
    或者
    但在，使用该命令有一个问题。当多个窗口同时使用该命令进入该容器时，所有的窗口都会同步显示。如果有一个窗口阻塞了，那么其他窗口也无法再进行操作。
    因为这个原因，所以docker attach命令不太适合于生产环境，平时自己开发应用时可以使用该命令。
    
    2、docker exec -it 容器ID /bin/bash 
    或者
    
    3、docker exec -it 容器的name bash
    退出容器命令
    
    4、exit【退出】
    
    5、Ctrl+P+Q 【退出】   
    
    注:如果出现
    OCI runtime exec failed: exec failed: container_linux.go:345: 
    starting container process caused "exec: \"/bin/bash\": 
    stat /bin/bash: no such file or directory": unknown
    
    这样的错误，是我们docker镜像中的/bin/bash文件并不存在，可能存在的是/bin/sh文件，使用
    docker exec -it 44fc0f0582d9 /bin/sh 即可
    
    firewall-cmd --state # 查看防火墙状态
    systemctl stop firewalld.service #关闭防火墙
    systemctl disable firewalld.service   #禁止firewall开机启动