# springboot启动时指定jar包内部或者外部的配置

## 1. 直接启动jar包

#### 1.2 指定jar包内配置文件

做这个的前提是你打jar包时里面就包含了不同环境配置的配置文件，主要的问题在于用`-Dspring.profiles.active`还是`--spring.profiles.active=dev`，这两个使用上有区别。

命令：

```shell
java -jar -Dspring.profiles.active=dev {your jar}.jar
```

`-Dspring.profiles.active=dev`如果放到后面，无效，但是不会报错，此时加载的就是打包时预设置的配置文件。



或者命令：

```shell
java -jar {your jar}.jar --spring.profiles.active=dev
```

`--spring.profiles.active=dev`如果放到前面，直接报错【有争议，有时候可以】

**目前发现一个特例**，Dockerfile里面使用ENTRYPOINT时`-Dspring.profiles.active=dev`不讲究位置在哪，使用CMD时不行。

例子：【linux系统】gjprod为外部配置文件

```shell
nohup java -jar wex_trade_gj.jar --spring.profiles.active=gjprod > wex_trade_gj.log &
```

#### 1.2 直接指定配置文件，不使用jar包内置的配置文件

这个其实和上面的类似。
  命令：

java -jar {your jar}.jar --spring.config.location={file path}

或者命令：

java -jar -Dspring.config.location={file path} {your jar}.jar

例子：【windows系统】

```java
java –jar -Dspring.config.location=E:\zzlh_workplace\wx_trade\target\application-gjdev.properties wex_trade.jar
```

