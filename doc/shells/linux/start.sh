#!/bin/bash

# 操作名
ACTION=$1
# 应用名
MODULER=$2
# 环境变量
PROFILES=$3
# 日志文件
FILE=$4


if [ "$ACTION" = "" ];
then
    echo -e "\033[0;31m 未输入操作名 \033[0m  \033[0;34m {start|stop|restart|status} \033[0m"
    exit 1
fi

if [ "$MODULER" = "" ];
then
    echo -e "\033[0;31m 未输入应用名 \033[0m"
    exit 1
fi
if [ "$PROFILES" = "" ];
then
    PROFILES="prod"
fi

if [ "$FILE" = "" ];
then
    FILE="./logs/$MODULER/$MODULER_log.txt"
fi

# Xms是设定程序启动时占用内存大小，大点，程序会启动的快一点
# Xmx是设定程序运行期间最大可占用的内存大小，超出了这个设置值，就会抛出OutOfMemory异常
# Xss是指设定每个线程的堆栈大小
JAVA_OPT="-server -Xms256M -Xmx256M -Xss512k -XX:MetaspaceSize=64M -XX:MaxMetaspaceSize=128M -XX:+UseG1GC"
JAVA_OPT=$JAVA_OPT" -Dspring.profiles.active=$PROFILES"
echo $JAVA_OPT


function start()
{
	count=`ps -ef | grep java | grep $MODULER | grep -v grep | wc -l`
	if [ $count != 0 ];then
		echo "$MODULER is running..."
	else
		echo "Start $MODULER success..."
		# nohup java -jar $JAVA_OPT $MODULER.jar $MODULER > /dev/null 2>&1 &
		nohup java -jar $JAVA_OPT ../package/$MODULER.jar $MODULER > ../logs/$FILE 2>&1 &
	fi
}

function stop()
{
	echo "Stop $MODULER"
	pid=`ps -ef | grep java | grep $MODULER | grep -v grep | awk '{print $2}'`
	count=`ps -ef | grep java | grep $MODULER | grep -v grep | wc -l`

	if [ $count != 0 ];then
	    kill $pid
    	count=`ps -ef | grep java | grep $MODULER | grep -v grep | wc -l`

      pid=`ps -ef | grep java | grep $MODULER | grep -v grep | awk '{print $2}'`
      kill -9 $pid

      echo "Stop $MODULER Success"
	fi
}

function restart()
{
	stop
	sleep 2
	start
}

function status()
{
    count=`ps -ef | grep java | grep $MODULER | grep -v grep | wc -l`
    if [ $count != 0 ];then
        echo "$MODULER is running..."
    else
        echo "$MODULER is not running..."
    fi
}

case $ACTION in
	start)
	start;;
	stop)
	stop;;
	restart)
	restart;;
	status)
	status;;
	*)

	echo -e "\033[0;31m Usage: \033[0m  \033[0;34m sh  $0  {start|stop|restart|status}  {SpringBootJarName} \033[0m
\033[0;31m Example: \033[0m
	  \033[0;33m sh  $0  start esmart-test.jar \033[0m"
esac
