package com.zhl;

import com.zhl.util.RedisUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author 凌晨
 * @Title: RedisServiceTest
 * @Description TODO
 * @date： 2020/8/12 15:06
 * @version： V1.0
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RedisServiceTest {

    @Autowired
    private RedisUtil redisUtil;

    @Test
    public void test(){
        String key = "测试001";
        String value = "哈哈哈哈哈哈ddddd哈";
        boolean set = redisUtil.set(key, value, 30);
        System.out.println(set);

        String str = (String)redisUtil.get(key);
        System.out.println(key+"值为："+str);

        long expire = redisUtil.getExpire(key);
        System.out.println(key+"过期时间为："+expire);

    }

    @Test
    public void test2(){
        String key = "token:0be188f4755549b39f0f8eefd65f88fd";
        boolean b = redisUtil.hasKey(key);
        System.out.println(b);
        String str = (String)redisUtil.get(key);
        System.out.println(key+"值为："+str);

        long expire = redisUtil.getExpire(key);
        System.out.println(key+"过期时间为："+expire);

    }

}
