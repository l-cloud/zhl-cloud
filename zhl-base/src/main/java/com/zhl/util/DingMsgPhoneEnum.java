package com.zhl.util;

import lombok.Getter;

/**
 * @author zhl
 * @Title: DingMsgPhoneEnum
 * @Description 钉钉消息接收用户，配置钉钉绑定的电话即可
 * @date： 2020/10/21 14:51
 * @version： V1.0
 */
@Getter
public enum DingMsgPhoneEnum {
    GENERAL_PURPOSE("138****0741,156****7257,150****8011,188****535", "通用（包含技术，产品，领导等）"),
    DEVELOPER_PHONE("138****0741,156****7257,150****8011,188****3535", "技术人员"),
    PRODUCT_PERSONNEL_PHONE("", "产品人员"),
    DATA_ANALYST_PHONE("", "数据分析人员");

    private String phone;

    private String name;

    DingMsgPhoneEnum(String phone, String name) {
        this.phone = phone;
        this.name = name;
    }
}
