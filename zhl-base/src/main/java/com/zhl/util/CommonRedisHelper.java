package com.zhl.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.Objects;

/**
 * @author 凌晨
 * @Title: CommonRedisHelper
 * @Description 使用RedisTemplate的execute的回调方法，里面使用Setnx方法
 *              Setnx就是，如果没有这个key，那么就set一个key-value,
 *              但是如果这个key已经存在，那么将不会再次设置，get出来的value还是最开始set进去的那个value.
 * @date： 2020/8/6 21:44
 * @version： V1.0
 */
public class CommonRedisHelper {
    //锁名称
    public static final String LOCK_PREFIX = "redis_lock";
    //加锁失效时间，毫秒
    public static final int LOCK_EXPIRE = 300; // ms

    @Autowired
    RedisTemplate redisTemplate;

    /**
     *  最终加强分布式锁
     *
     * @param key key值
     * @return 是否获取到
     */
    public boolean lock(String key){
        String lock = LOCK_PREFIX + key;
        // 利用lambda表达式
        return (Boolean) redisTemplate.execute((RedisCallback) connection -> {
            long expireAt = System.currentTimeMillis() + LOCK_EXPIRE + 1;
            Boolean acquire = connection.setNX(lock.getBytes(), String.valueOf(expireAt).getBytes());
            if (acquire) {
                return true;
            } else {
                byte[] value = connection.get(lock.getBytes());
                if (Objects.nonNull(value) && value.length > 0) {

                    long expireTime = Long.parseLong(new String(value));
                    // 如果锁已经过期
                    if (expireTime < System.currentTimeMillis()) {
                        // 重新加锁，防止死锁
                        byte[] oldValue = connection.getSet(lock.getBytes(), String.valueOf(System.currentTimeMillis() + LOCK_EXPIRE + 1).getBytes());
                        return Long.parseLong(new String(oldValue)) < System.currentTimeMillis();
                    }
                }
            }
            return false;
        });
    }

    /**
     * 删除锁
     *
     * @param key
     */
    public void delete(String key) {
        redisTemplate.delete(key);
    }

//    public static void main(String[] args) {
//        CommonRedisHelper redisHelper = new CommonRedisHelper();
//        boolean lock = redisHelper.lock(key);
//        if (lock) {
//            // 执行逻辑操作
//            redisHelper.delete(key);
//        } else {
//            // 设置失败次数计数器, 当到达5次时, 返回失败
//            int failCount = 1;
//            while(failCount <= 5){
//                // 等待100ms重试
//                try {
//                    Thread.sleep(100l);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//                if (redisHelper.lock(key)){
//                    // 执行逻辑操作
//                    redisHelper.delete(key);
//                }else{
//                    failCount ++;
//                }
//            }
//            throw new RuntimeException("现在创建的人太多了, 请稍等再试");
//        }
//    }
}
