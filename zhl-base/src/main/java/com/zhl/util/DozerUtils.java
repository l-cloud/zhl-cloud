package com.zhl.util;


import org.dozer.DozerBeanMapperBuilder;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * 很诡异，DozerUtils 工具类不能以 xxMapper 结尾
 * <p>
 * 使用dozer对复杂对象进行转换时，若对象标记了 @Accessors(chain = true) 注解， 会报NPE异常
 *
 * @author zuihou
 * @createTime 2017-12-08 14:41
 */
@Component
public class DozerUtils {

    @Autowired
    private Mapper mapper;

    public DozerUtils(Mapper mapper) {
        this.mapper = mapper;
    }

    public Mapper getMapper() {
        return this.mapper;
    }

    /**
     * Constructs new instance of destinationClass and performs mapping between from source
     *
     * @param source
     * @param destinationClass
     * @param <T>
     * @return
     */
    public <T> T map(Object source, Class<T> destinationClass) {
        if (source == null) {
            return null;
        }
        return mapper.map(source, destinationClass);
    }

    public <T> T map2(Object source, Class<T> destinationClass) {
        if (source == null) {
            try {
                return destinationClass.newInstance();
            } catch (Exception e) {
            }
        }
        return mapper.map(source, destinationClass);
    }

    /**
     * Performs mapping between source and destination objects
     *
     * @param source
     * @param destination
     */
    public void map(Object source, Object destination) {
        if (source == null) {
            return;
        }
        mapper.map(source, destination);
    }

    /**
     * Constructs new instance of destinationClass and performs mapping between from source
     *
     * @param source
     * @param destinationClass
     * @param mapId
     * @param <T>
     * @return
     */
    public <T> T map(Object source, Class<T> destinationClass, String mapId) {
        if (source == null) {
            return null;
        }
        return mapper.map(source, destinationClass, mapId);
    }

    /**
     * Performs mapping between source and destination objects
     *
     * @param source
     * @param destination
     * @param mapId
     */
    public void map(Object source, Object destination, String mapId) {
        if (source == null) {
            return;
        }
        mapper.map(source, destination, mapId);
    }

    /**
     * 将集合转成集合
     * List<A> -->  List<B>
     *
     * @param sourceList       源集合
     * @param destinationClass 待转类型
     * @param <T>
     * @return
     */
    public <T, E> List<T> mapList(Collection<E> sourceList, Class<T> destinationClass) {
        return mapPage(sourceList, destinationClass);
    }


    public <T, E> List<T> mapPage(Collection<E> sourceList, Class<T> destinationClass) {
        if (sourceList == null || sourceList.isEmpty() || destinationClass == null) {
            return Collections.emptyList();
        }
        List<T> destinationList = sourceList.parallelStream()
                .filter(item -> item != null)
                .map((sourceObject) -> mapper.map(sourceObject, destinationClass))
                .collect(Collectors.toList());

        return destinationList;
    }

    public <T, E> Set<T> mapSet(Collection<E> sourceList, Class<T> destinationClass) {
        if (sourceList == null || sourceList.isEmpty() || destinationClass == null) {
            return Collections.emptySet();
        }
        return sourceList.parallelStream().map((sourceObject) -> mapper.map(sourceObject, destinationClass)).collect(Collectors.toSet());
    }

    public static void main(String[] args) {

     // withMappingFiles 方法加载 xml 配置文件，多个配置文件可以用 "," 隔开，如 withMappingFiles("userMapping.xml" , "anotherMapping.xml")
        Mapper mapper = DozerBeanMapperBuilder.create().withMappingFiles("dozer/userMapping.xml").build();
        DozerUtils dozerUtil = new DozerUtils(mapper);
//        User user = new User();
//        user.setName("西门打赏");
//        user.setAge(12);
//        user.setFlag(true);
//        user.setPassword("123123");
//        user.setTtt("tttttttttttttt");
//        MyUser user1 = dozerUtil.map(user,MyUser.class);
//        System.out.println(user1);

//        test1();
    }

//    Dozer 是 Java Bean 到 Java Bean 的映射器，
//    他以递归的方式将数据从一个对象复制到另一个对象。Dozer 支持简单属性映射、双向映射、隐式映射以及递归映射。
//    使用该映射器可以很方便的在项目中进行 pojo、do、vo 之间的转换。
//    这是 Dozer 的默认映射方式——隐式映射，Dozer 自动的将两个实体类的相同属性名的属性进行映射。
//    如果两个属性的属性名相同，但是类型不同，Dozer 会按照默认的转换规则进行类型的转换，而且不同修饰符的属性也能正常进行映射
    private static void test1() {
        Mapper mapper = DozerBeanMapperBuilder.buildDefault();
        DozerUtils dozerUtil = new DozerUtils(mapper);
//        User user = new User();
//        user.setName("西门打赏");
//        user.setAge(12);
//        user.setFlag(true);
//        user.setPassword("123123");
//        MyUser user1 = dozerUtil.map(user,MyUser.class);
//        System.out.println(user1);
    }
}
