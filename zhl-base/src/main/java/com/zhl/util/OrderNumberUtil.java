package com.zhl.util;


import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author gourd.hu
 */
@Component
public class OrderNumberUtil {

    @Autowired
    private RedisUtil redisUtil;

    /**
     * 获取当天的自增订单
     * @return
     */
    public String getOrderNumber(){
        String nowDateStr = DateTimeFormatter.ofPattern(DateUtils.DATE_FORMAT_YYYYMMDD).format(LocalDateTime.now());
        long number = 1;
        if(redisUtil.existAny(nowDateStr)){
            number = redisUtil.incr(nowDateStr,1);
        }else {
            redisUtil.setExpire(nowDateStr,String.valueOf(number),24*60*60L);
        }
        return nowDateStr + StringUtils.leftPad(String.valueOf(number), 4, '0');
    }


}
