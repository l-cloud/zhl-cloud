package com.zhl.util;

import lombok.Getter;

/**
 * @author zhl
 * @Title: DingTokenEnum
 * @Description 钉钉消息群机器人access_token、secret
 * @date： 2020/10/21 14:45
 * @version： V1.0
 */
@Getter
public enum  DingTokenEnum {
    SEND_SMS_GROUP("替换成你自己机器人的access_token", "替换成你自己机器人的secret", "短信发送异常通知"),
    SEND_SMS_BY_MARKET_IMPORT_TOKEN("替换成你自己机器人的access_token", "替换成你自己机器人的secret", "导入通知消息"),
    SEND_SMS_BY_PRE_CASE_WARN_TOKEN("替换成你自己机器人的access_token","替换成你自己机器人的secret",  "预警消息通知"),
    SEND_SMS_BY_MARKDOWN_TOKEN("替换成你自己机器人的access_token", "替换成你自己机器人的secret", "系统消息通知，markdown类型"),
    SEND_SMS_BY_DEVELOPER_TOKEN("替换成你自己机器人的access_token", "替换成你自己机器人的secret", "系统消息通知，技术专用"),
    SEND_SMS_TEST_TOKEN("38bca74d6877d51fdd36b5b81fc218ba36a3d8ccb38d25a2b4a96f09f24288c2", "替换成你自己机器人的secret", "系统消息通知，技术专用");

    private String token;  //机器人 Webhook 地址中的 access_token

    private String secret;  //密钥，机器人安全设置页面，加签一栏下面显示的SEC开头的字符串

    private String name;

    DingTokenEnum(String token, String secret, String name) {
        this.token = token;
        this.secret = secret;
        this.name = name;
    }
}
