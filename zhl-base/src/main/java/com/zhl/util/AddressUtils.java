package com.zhl.util;

import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 获取地址类
 * 
 * @author ruoyi
 */
public class AddressUtils
{
    private static final Logger log = LoggerFactory.getLogger(AddressUtils.class);

    // http://ip.taobao.com/service/getIpInfo.php
    public static final String IP_URL = "https://m.so.com/position";

    public static String getRealAddressByIP(String ip)
    {
        String address = "";
        try
        {
            address = HttpUtils.sendGet(IP_URL, "ip=" + ip);
            JSONObject json = JSONObject.parseObject(address);
            JSONObject object = json.getObject("data", JSONObject.class);
            JSONObject position = object.getJSONObject("position");
            String region = position.getString("province");
            String city = position.getString("city");
            address = region + " " + city;
        }
        catch (Exception e)
        {
            log.error("获取地理位置异常:", e);
        }
        return address;
    }

    public static void main(String[] args) {
        String realAddressByIP = getRealAddressByIP("47.105.165.159");
        System.out.println(realAddressByIP);

    }
}
