package com.zhl.model.sys.user.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.zhl.common.sensitive.SensitiveInfo;
import com.zhl.common.sensitive.SensitiveType;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author zhl
 * @Title: User
 * @Description TODO
 * @date： 2020/8/3 13:34
 * @version： V1.0
 */
@Data
@Accessors(chain = true)
public class User {
    private Long id;
    /**创建时间*/
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    /**姓名*/
    @SensitiveInfo(SensitiveType.CHINESE_NAME)
    private String username;
    /**姓名*/
    @SensitiveInfo(SensitiveType.CHINESE_NAME)
    private String realname;
    /**密码*/
    private String password;
    /**部门code*/
    private String orgCode;
    /**年龄*/
    private Integer age;
    /**身份证*/
    @SensitiveInfo(SensitiveType.ID_CARD)
    private String idCard;
    /**移动电话*/
    @SensitiveInfo(SensitiveType.MOBILE_PHONE)
    private String mobile;
    /**住址*/
    private String address;
    /**生日*/
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date birthday;
    /**性别（1：男 2：女）*/
    private Integer sex;
    /**电子邮件*/
    @SensitiveInfo(SensitiveType.EMAIL)
    private String email;
    /**微信号*/
    private String wx;
    /**qq号*/
    private String qq;
    /**状态(1：正常 2：冻结 ）*/
    private Integer status;

}
