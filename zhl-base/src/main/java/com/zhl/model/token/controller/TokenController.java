package com.zhl.model.token.controller;

import com.zhl.common.vo.Result;
import com.zhl.common.vo.ServerResponse;
import com.zhl.model.token.service.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 凌晨
 * @Title: TokenController
 * @Description TODO
 * @date： 2020/8/12 14:05
 * @version： V1.0
 */
@RestController
@RequestMapping("/token")
public class TokenController {

    @Autowired
    private TokenService tokenService;

//    @GetMapping
//    public ServerResponse token() {
//        return tokenService.createToken();
//    }

    @GetMapping
    public Result token() {
        return tokenService.createToken();
    }

}
