package com.zhl.model.token.controller;

import com.zhl.common.annotation.AccessLimit;
import com.zhl.common.annotation.ApiIdempotent;
import com.zhl.common.vo.Result;
import com.zhl.common.vo.ServerResponse;
import com.zhl.model.token.service.TestService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
@Slf4j
public class TestController {

    @Autowired
    private TestService testService;

//    @ApiIdempotent
//    @PostMapping("testIdempotence")
//    public ServerResponse testIdempotence() {
//        return testService.testIdempotence();
//    }

    @ApiIdempotent
    @PostMapping("testIdempotence")
    public Result testIdempotence() {
        return testService.testIdempotence();
    }

    @AccessLimit(maxCount = 5, seconds = 5)
    @PostMapping("accessLimit")
    public Result accessLimit() {
        return testService.accessLimit();
    }

//    @AccessLimit(maxCount = 5, seconds = 5)
//    @PostMapping("accessLimit")
//    public ServerResponse accessLimit() {
//        return testService.accessLimit();
//    }

}
