package com.zhl.model.token.service;

import com.zhl.common.vo.Result;
import com.zhl.common.vo.ServerResponse;

public interface TestService {

//    ServerResponse testIdempotence();
//
//    ServerResponse accessLimit();

    Result testIdempotence();

    Result accessLimit();

}
