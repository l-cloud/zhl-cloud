package com.zhl.model.token.service;


import com.zhl.common.vo.Result;
import com.zhl.common.vo.ServerResponse;

import javax.servlet.http.HttpServletRequest;

public interface TokenService {

//    ServerResponse createToken();

    Result createToken();

    void checkToken(HttpServletRequest request);

}
