package com.zhl.model.token.service.impl;
import com.zhl.common.vo.Result;
import com.zhl.common.vo.ServerResponse;
import com.zhl.model.token.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TestServiceImpl implements TestService {

//    @Override
//    public ServerResponse testIdempotence() {
//        return ServerResponse.success("testIdempotence: success");
//    }
//
//    @Override
//    public ServerResponse accessLimit() {
//        return ServerResponse.success("accessLimit: success");
//    }

    @Override
    public Result testIdempotence() {
        return Result.ok("testIdempotence: success");
    }

    @Override
    public Result accessLimit() {
        return Result.ok("accessLimit: success");
    }

}
