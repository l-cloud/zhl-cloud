package com.zhl.model.log.controller;

import com.zhl.common.vo.Result;
import com.zhl.model.log.service.ISysLogService;
import com.zhl.model.log.vo.SysLogVo;
import com.zhl.model.sys.user.entity.User;
import com.zhl.util.UserUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

 /**
 * @Title: Controller
 * @Description: 系统日志信息
 * @date：   2019-12-27
 * @version： V1.0
 */
@RestController
@RequestMapping("api/sys/log")
@Slf4j
@Api(tags = "系统日志模块")
public class SysLogController {
	@Autowired
	private ISysLogService sysLogService;


	 /**
	  * 分页列表查询
	  * @param pageNo 页码
	  * @param pageSize 每页大小
	  * @param startDate 开始日期
	  * @param endDate   结束日期
	  * @param os 操作系统
	  * @param username 用户账号
	  * @param realname 用户名称
	  * @param logContent 日志内容
	  * @param ip ip地址
	  * @return
	  */
	@GetMapping(value = "/list")
	@ApiOperation(value = "系统日志列表")
	@ApiImplicitParams({@ApiImplicitParam(name = "pageNo",value = "页数",required = false,paramType = "query"),
			@ApiImplicitParam(name = "pageSize",value = "每页大小",required = false,paramType = "query"),
			@ApiImplicitParam(name = "startDate",value = "开始日期",required = false,paramType = "query"),
			@ApiImplicitParam(name = "endDate",value = "结束日期",required = false,paramType = "query"),
			@ApiImplicitParam(name = "os",value = "操作系统",required = false,paramType = "query"),
			@ApiImplicitParam(name = "username",value = "用户账号",required = false,paramType = "query"),
			@ApiImplicitParam(name = "realname",value = "用户名称",required = false,paramType = "query"),
			@ApiImplicitParam(name = "logContent",value = "日志内容",required = false,paramType = "query"),
			@ApiImplicitParam(name = "ip",value = "IP",required = false,paramType = "query")
	})
	public Result<IPage<SysLogVo>> queryPageList(@RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
												 @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
												 @RequestParam(name="startDate", required = false) String startDate,
												 @RequestParam(name="endDate", required = false) String endDate,
												 @RequestParam(name="os", required = false) String os,
												 @RequestParam(name="username", required = false) String username,
												 @RequestParam(name="realname", required = false) String realname,
												 @RequestParam(name="logContent", required = false) String logContent,
												 @RequestParam(name="ip", required = false) String ip) {
		Result<IPage<SysLogVo>> result = new Result<IPage<SysLogVo>>();
		User user = UserUtil.getLoginUser();
		QueryWrapper queryWrapper = new QueryWrapper();
		queryWrapper.eq("is_deleted",0);
		queryWrapper.eq("operate_org_code",user.getOrgCode());
		Page<SysLogVo> page = new Page<SysLogVo>(pageNo,pageSize);
		if(StringUtils.isNotBlank(startDate)){
			queryWrapper.gt("create_time",startDate);
		}
		if(StringUtils.isNotBlank(endDate)){
			queryWrapper.lt("create_time",endDate);
		}
		if(StringUtils.isNotBlank(username)){
			queryWrapper.like("username",username);
		}
		if(StringUtils.isNotBlank(realname)){
			queryWrapper.like("realname",realname);
		}
		if(StringUtils.isNotBlank(logContent)){
			queryWrapper.like("log_content",logContent);
		}
		if(StringUtils.isNotBlank(ip)){
			queryWrapper.like("ip",ip);
		}
		if(StringUtils.isNotBlank(os)){
			queryWrapper.like("os",os);
		}
		IPage<SysLogVo> pageList = sysLogService.queryPageList(page, queryWrapper);
		result.setSuccess(true);
		result.setCode(200);
		result.setResult(pageList);
		return result;
	}
	
}
