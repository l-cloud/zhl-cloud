package com.zhl.model.log.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * @Description: 系统日志信息
 * @date：   2019-12-27
 * @version： V1.0
 */
@Data
@TableName("sys_log")
public class SysLog implements Serializable {
    private static final long serialVersionUID = 1L;
    
	/**id*/
	@TableId(type = IdType.UUID)
	private String id;

	/**日志类型（1登录日志，2操作日志）*/
	private Integer logType;

	@ApiModelProperty(value = "模块名")
	private String moduleName;

	@ApiModelProperty(value = "业务名")
	private String businessName;

	@ApiModelProperty(value = "描述")
	private String description;

	/**操作类型*/
	private String operateType;

	/**操作部门code*/
	private String operateOrgCode;

	/**日志内容*/
	private String logContent;
	/**操作用户账号*/
	private String username;
	/**操作用户名称*/
	private String realname;
	/**请求java方法*/
	private String method;
	/**请求路径*/
	private String requestUrl;
	/**请求参数*/
	private Object requestParam;
	/**请求类型*/
	private String requestType;
	/**耗费时间*/
	private Long costTime;
	/**地址*/
	private String location;
	/**ip*/
	private String ip;
	/**浏览器类型*/
	private String browser;
	/**操作系统*/
	private String os;
	/**备注*/
	private String remarks;

	@ApiModelProperty(value = "返回结果")
	private String responseDetail;

	@ApiModelProperty(value = "异常详细")
	private String exceptionDetail;

	@ApiModelProperty(value = "过期删除时间")
	private Long expireTime;

	/**0: 未删除 1：删除*/
	private Integer isDeleted;
	/**创建时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private Date createTime;
	/**创建人*/
	private String createBy;
	/**更新时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private Date updateTime;
	/**更新人*/
	private String updateBy;
}
