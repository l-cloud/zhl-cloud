package com.zhl.model.log.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zhl.model.log.entity.SysLog;
import com.zhl.model.log.vo.ExcelSysLog;
import com.zhl.model.log.vo.SysLogVo;
import org.aspectj.lang.ProceedingJoinPoint;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @Description: 系统日志信息
 * @date：   2019-12-27
 * @version： V1.0
 */
public interface ISysLogService extends IService<SysLog> {


    IPage<SysLogVo> queryPageList(Page<SysLogVo> page, QueryWrapper queryWrapper);


    /**
     * 获取操作日志信息,仅供excel导出使用
     * @param queryWrapper
     * @return
     */
    List<ExcelSysLog> querySysLogExcelList(QueryWrapper queryWrapper);

    void asyncSaveLog(ProceedingJoinPoint joinPoint,Long time,SysLog sysLog);

}
