package com.zhl.model.log.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description: 系统日志信息
 * @date：   2019-12-27
 * @version： V1.0
 */
@Data
public class SysLogVo implements Serializable {
    private static final long serialVersionUID = 1L;


    /**ID*/
    private String id;
	/**日志内容*/
	private String logContent;
	/**操作用户id*/
	private String username;
	/**操作用户名称*/
	private String realname;
	/**地址*/
	private String location;
	/**ip*/
	private String ip;
	/**浏览器类型*/
	private String browser;
	/**操作系统*/
	private String os;
	/**创建时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private Date createTime;
}
