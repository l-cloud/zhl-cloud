package com.zhl.model.log.vo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.io.Serializable;

/**
 * 操作日志导出类
 */
@Data
public class ExcelSysLog implements Serializable {

    public ExcelSysLog() {
    }

    /**日志内容*/
    @Excel(name = "日志内容", orderNum = "1", width = 25)
    private String logContent;

    /**操作用户id*/
    @Excel(name = "用户ID", orderNum = "1", width = 25)
    private String username;

    /**操作用户名称*/
    @Excel(name = "用户名称", orderNum = "2", width = 15)
    private String realname;

    /**ip*/
    @Excel(name = "IP", orderNum = "3", width = 15)
    private String ip;

    /**地址*/
    @Excel(name = "地址", orderNum = "4", width = 40)
    private String location;

    /**浏览器类型*/
    @Excel(name = "浏览器", orderNum = "5", width = 25)
    private String browser;

    /**操作系统*/
    @Excel(name = "操作系统", orderNum = "6", width = 25)
    private String os;

    /**创建时间*/
    @Excel(name = "操作时间", format = "yyyy-MM-dd HH:mm:ss", orderNum = "7", width = 25)
    private String createTime;

}
