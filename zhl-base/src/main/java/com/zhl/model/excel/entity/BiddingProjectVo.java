package com.zhl.model.excel.entity;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelTarget;
import lombok.Data;

/**
 * @author zhl
 * @Title: BiddingProjectVo
 * @Description TODO
 * @date： 2020/9/17 10:13
 * @version： V1.0
 */
@Data
@ExcelTarget("zzgjExport") //这个是作用于最外层的对象,描述这个对象的id,以便支持一个对象可以针对不同导出做出不同处理
public class BiddingProjectVo {

    /**项目名称*/
    @Excel(name="项目名称",width = 55) //作用到filed上面,是对Excel一列的一个描述
    private String projectName;
    /**业务编号*/
    @Excel(name="业务编号",width = 25)
    private String businessNumber;
    /**包业务编号*/
    @Excel(name="包业务编号",width = 25)
    private String packageBusinessNumber;
    /**包名称*/
    @Excel(name="包名称",width = 55)
    private String packageName;
    /**部门名称*/
    @Excel(name="部门名称",width = 15)
    private String departName;
    /**开标方式*/
    @Excel(name="开标方式")
    private String bidOpeningType;
    /**项目经理*/
    @Excel(name="项目经理")
    private String projectManager;
    /**手机号*/
    @Excel(name="手机号",width = 18)
    private String phone;
    /**开标时间*/
    @Excel(name="开标时间",format = "yyyy-MM-dd HH:mm:ss",width = 22)
    private String bidOpeningTime;
    /**立项时间*/
    @Excel(name="立项时间",format = "yyyy-MM-dd HH:mm:ss",width = 22)
    private String approvalTime;
    /**招标人*/
    @Excel(name="招标人",width = 25)
    private String tendereeName;
    /**项目状态*/
    @Excel(name="项目状态",width = 15)
    private String projectStatus;
    /**售标结束时间*/
    @Excel(name="售标结束时间",format = "yyyy-MM-dd HH:mm:ss",width = 22)
    private String endTime;

}
