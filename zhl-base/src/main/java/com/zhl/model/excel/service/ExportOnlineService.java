package com.zhl.model.excel.service;

import com.zhl.model.excel.entity.BiddingProjectVo;

import java.util.List;
import java.util.Map;

public interface ExportOnlineService {

    List<BiddingProjectVo> queryProjectInfo(Map<String,Object> param);
}
