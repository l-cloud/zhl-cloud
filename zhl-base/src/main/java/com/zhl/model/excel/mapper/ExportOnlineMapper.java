package com.zhl.model.excel.mapper;

import com.zhl.model.excel.entity.BiddingProjectVo;

import java.util.List;
import java.util.Map;

public interface ExportOnlineMapper {

    List<BiddingProjectVo> queryProjectInfo(Map<String,Object> data);
}
