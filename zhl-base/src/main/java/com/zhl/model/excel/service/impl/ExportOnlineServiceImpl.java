package com.zhl.model.excel.service.impl;

import com.zhl.model.excel.entity.BiddingProjectVo;
import com.zhl.model.excel.mapper.ExportOnlineMapper;
import com.zhl.model.excel.service.ExportOnlineService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author zhl
 * @Title: ExportOnlineServiceImpl
 * @Description TODO
 * @date： 2020/9/17 13:41
 * @version： V1.0
 */
@Slf4j
@Service
public class ExportOnlineServiceImpl implements ExportOnlineService {

    @Autowired
    private ExportOnlineMapper exportOnlineMapper;

    @Override
    public List<BiddingProjectVo> queryProjectInfo(Map<String,Object> param) {
        return exportOnlineMapper.queryProjectInfo(param);
    }
}
