package com.zhl.common.annotation;

import java.lang.annotation.*;

/**
 * 防止表单重复提交 注解
 *
 * @author mzk
 */

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface NoRepeatSubmit {

    int lockTime() default 3;
}
