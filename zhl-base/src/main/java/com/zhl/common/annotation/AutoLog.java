package com.zhl.common.annotation;



import com.zhl.common.constant.CommonConstant;

import java.lang.annotation.*;

/**
 * 系统日志注解
 * 
 * @author zhl
 * @email 852039873@qq.com
 * @date 2019年1月14日
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface AutoLog {

	/**
	 * 日志内容
	 * 
	 * @return
	 */
	String value() default "";

	/**
	 * 日志类型
	 * 
	 * @return 0:操作日志;1:登录日志;2:定时任务;
	 */
	int logType() default CommonConstant.LOG_TYPE_2;

	/**
	 * 描述
	 * @return
	 */
	String description() default "";

	/**
	 * 模块名
	 * @return
	 */
	String moduleName() default "";

	/**
	 * 业务名
	 * @return
	 */
	String businessName() default "";
}
