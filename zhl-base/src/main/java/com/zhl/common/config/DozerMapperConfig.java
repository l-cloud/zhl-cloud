package com.zhl.common.config;

import com.github.dozermapper.spring.DozerBeanMapperFactoryBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;

import java.io.IOException;

/**
 * @author 凌晨
 * @Title: DozerMapperConfig
 * @Description TODO
 * @date： 2020/10/18 17:39
 * @version： V1.0
 */
//从版本 5.3.2 开始，Dozer 也开始提供注解支持，使用注释的明显原因是避免在映射代码中复制字段和方法名称，
// 注释可以放在映射类的属性上，从而减少代码量。 @Mapping("name")
// 但是有些情况应该减少使用注解，甚至无法使用注解，如：
//        你正在映射类时，这些类不在你的控制下，但在库中提供
//        映射类非常复杂，而且需要许多配置
//    可以混合api方式、xml方式、注解方式进行类的映射
//    注意：对于实际应用程序，建议不要在每次映射对象时创建一个新的 Mapper 实例，
//    而是重新使用上次创建的 Mapper 实例，可以把 Mapper 封装成单例模式使用。
@Configuration
public class DozerMapperConfig {

    @Bean
    public DozerBeanMapperFactoryBean dozerMapper(@Value("classpath:mapping/*.xml") Resource[] resources) throws IOException {
        DozerBeanMapperFactoryBean dozerBeanMapperFactoryBean = new DozerBeanMapperFactoryBean();
        dozerBeanMapperFactoryBean.setMappingFiles(resources);
        return dozerBeanMapperFactoryBean;
    }
}
