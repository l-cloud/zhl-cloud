package com.zhl.common.config;

public class GobleException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public GobleException(String message){
		super(message);
	}

	public GobleException(Throwable cause)
	{
		super(cause);
	}

	public GobleException(String message, Throwable cause)
	{
		super(message,cause);
	}
}
