package com.zhl.common.config;

import com.zhl.model.sys.user.entity.User;
import com.zhl.util.MyConvertUtil;
import com.zhl.util.UserUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.binding.MapperMethod.ParamMap;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlCommandType;
import org.apache.ibatis.plugin.*;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.util.Date;
import java.util.Properties;

/**
 * mybatis拦截器，自动注入创建人、创建时间、修改人、修改时间
 *
 * @Author zhouhailong
 * @Date 2019-12-24
 */
@Slf4j
@Component
@Intercepts({@Signature(type = Executor.class, method = "update", args = {MappedStatement.class, Object.class})})
public class MybatisInterceptor implements Interceptor {

    @Override
    public Object intercept(Invocation invocation) throws Throwable {
        MappedStatement mappedStatement = (MappedStatement) invocation.getArgs()[0];
        String sqlId = mappedStatement.getId();
        log.debug("------sqlId------" + sqlId);
        SqlCommandType sqlCommandType = mappedStatement.getSqlCommandType();
        Object parameter = invocation.getArgs()[1];
        log.debug("------sqlCommandType------" + sqlCommandType);

        if (parameter == null) {
            return invocation.proceed();
        }
        if (SqlCommandType.INSERT == sqlCommandType) {
            Field[] fields = MyConvertUtil.getAllFields(parameter);
            // 获取登录用户信息
            User user = null;
            try {
                user = UserUtil.getLoginUser();
            } catch (Exception e) {
                user = null;
            }
            for (Field field : fields) {

                log.debug("------field.name------" + field.getName());

                try {
                    if ("createBy".equals(field.getName())) {
                        field.setAccessible(true);
                        Object local_createBy = field.get(parameter);
                        field.setAccessible(false);
                        if (local_createBy == null || local_createBy.equals("")) {
                            String createBy = "dxt";
                            if (user != null) {
                                // 登录账号
                                createBy = user.getUsername();
                            }
                            if (MyConvertUtil.isNotEmpty(createBy)) {
                                field.setAccessible(true);
                                field.set(parameter, createBy);
                                field.setAccessible(false);
                            }
                        }
                    }

                    // 注入创建时间
                    if ("createTime".equals(field.getName())) {
                        field.setAccessible(true);
                        Object local_createDate = field.get(parameter);
                        field.setAccessible(false);
                        if (local_createDate == null || local_createDate.equals("")) {
                            field.setAccessible(true);
                            field.set(parameter, new Date());
                            field.setAccessible(false);
                        }
                    }

                    //注入部门编码
                    if ("orgCode".equals(field.getName())) {
                        field.setAccessible(true);
                        Object local_sysOrgCode = field.get(parameter);
                        field.setAccessible(false);
                        if (local_sysOrgCode == null || local_sysOrgCode.equals("")) {
                            String orgCode = "";
                            // 获取登录用户信息
                            if (user != null) {
                                // 登录账号
                                orgCode = user.getOrgCode();
                            }
                            if (MyConvertUtil.isNotEmpty(orgCode)) {
                                field.setAccessible(true);
                                field.set(parameter, orgCode);
                                field.setAccessible(false);
                            }
                        }
                    }
                    //首次添加更新人
                    if ("updateBy".equals(field.getName())) {
                        field.setAccessible(true);
                        Object local_updateBy = field.get(parameter);
                        field.setAccessible(false);
                        if (local_updateBy == null || local_updateBy.equals("")) {
                            String updateBy = "dxt";
                            // 获取登录用户信息
                            if (user != null) {
                                // 登录账号
                                updateBy = user.getUsername();
                            }
                            if (!MyConvertUtil.isEmpty(updateBy)) {
                                field.setAccessible(true);
                                field.set(parameter, updateBy);
                                field.setAccessible(false);
                            }
                        }
                    }

                    //首次添加更新时间
                    if ("updateTime".equals(field.getName())) {
                        field.setAccessible(true);
                        Object local_updateDate = field.get(parameter);
                        field.setAccessible(false);
                        if (local_updateDate == null || local_updateDate.equals("")) {
                            field.setAccessible(true);
                            field.set(parameter, new Date());
                            field.setAccessible(false);
                        }
                    }

                    //首次为未删除
                    if ("isDeleted".equals(field.getName())) {
                        field.setAccessible(true);
                        Object local_updateDate = field.get(parameter);
                        field.setAccessible(false);
                        if (local_updateDate == null || local_updateDate.equals("")) {
                            field.setAccessible(true);
                            field.set(parameter, 0);
                            field.setAccessible(false);
                        }
                    }

                } catch (Exception e) {
                }
            }
        }
        if (SqlCommandType.UPDATE == sqlCommandType) {
            Field[] fields = null;
            if (parameter instanceof ParamMap) {
                ParamMap<?> p = (ParamMap<?>) parameter;
                //update-begin-author:scott date:20190729 for:批量更新报错issues/IZA3Q--
                if (p.containsKey("et")) {
                    parameter = p.get("et");
                } else {
                    parameter = p.get("param1");
                }
                //update-end-author:scott date:20190729 for:批量更新报错issues/IZA3Q-

                //update-begin-author:scott date:20190729 for:更新指定字段时报错 issues/#516-
                if (parameter == null) {
                    return invocation.proceed();
                }
                //update-end-author:scott date:20190729 for:更新指定字段时报错 issues/#516-

                fields = MyConvertUtil.getAllFields(parameter);
            } else {
                fields = MyConvertUtil.getAllFields(parameter);
            }

            for (Field field : fields) {
                log.debug("------field.name------" + field.getName());
                try {
                    if ("updateBy".equals(field.getName())) {

//						// 获取登录用户信息
                        User user = null;
                        try {
                            user = UserUtil.getLoginUser();
                        } catch (Exception e) {
                            user = null;
                        }
                        if (user != null) {
                            // 登录账号
                            String updateBy = user.getUsername();
                            field.setAccessible(true);
                            field.set(parameter, updateBy);
                            field.setAccessible(false);
                        }
                    }
                    if ("updateTime".equals(field.getName())) {
                        field.setAccessible(true);
                        field.set(parameter, new Date());
                        field.setAccessible(false);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return invocation.proceed();
    }

    @Override
    public Object plugin(Object target) {
        return Plugin.wrap(target, this);
    }

    @Override
    public void setProperties(Properties properties) {
        // TODO Auto-generated method stub
    }

}
