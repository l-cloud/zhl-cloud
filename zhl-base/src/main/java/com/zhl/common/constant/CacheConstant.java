package com.zhl.common.constant;

/**
 * @author: huangxutao
 * @date: 2019-06-14
 * @description: 缓存常量
 */
public interface CacheConstant {

	/**
	 * 字典信息缓存
	 */
	String SYS_DICT_CACHE = "sys:cache:dict";
	/**
	 * 表字典信息缓存
	 */
	String SYS_DICT_TABLE_CACHE = "sys:cache:dictTable";

	/**
	 * 数据权限配置缓存
	 */
	String SYS_DATA_PERMISSIONS_CACHE = "sys:cache:permission:datarules";

	/**
	 * 缓存用户信息
	 */
	String SYS_USERS_CACHE = "sys:cache:user";

	/**
	 * 全部部门信息缓存
	 */
	String SYS_DEPARTS_CACHE = "sys:cache:depart:alldata";


	/**
	 * 全部部门ids缓存
	 */
	String SYS_DEPART_IDS_CACHE = "sys:cache:depart:allids";


	/**
	 * 测试缓存key
	 */
	String TEST_DEMO_CACHE = "test:demo";

	/**
	 * 字典信息缓存
	 */
	String SYS_DYNAMICDB_CACHE = "sys:cache:dbconnect:dynamic:";

	Integer EXPIRE_TIME_MINUTE = 60*5;// 过期时间, 60s, 一分钟
	Integer EXPIRE_TIME_HOUR = 60 * 60;// 过期时间, 一小时
	Integer EXPIRE_TIME_DAY = 60 * 60 * 24;// 过期时间, 一天
	String TOKEN_PREFIX = "token:";
	String MSG_CONSUMER_PREFIX = "consumer:";
	String ACCESS_LIMIT_PREFIX = "accessLimit:";

	/**
	 * oauth 缓存前缀
	 */
	String PROJECT_OAUTH_ACCESS = "pig_oauth:access:";

	/**
	 * oauth 缓存令牌前缀
	 */
	String PROJECT_OAUTH_TOKEN = "pig_oauth:token:";

	/**
	 * 验证码前缀
	 */
	String DEFAULT_CODE_KEY = "DEFAULT_CODE_KEY:";

	/**
	 * 菜单信息缓存
	 */
	String MENU_DETAILS = "menu_details";

	/**
	 * 用户信息缓存
	 */
	String USER_DETAILS = "user_details";

	/**
	 * 字典信息缓存
	 */
	String DICT_DETAILS = "dict_details";

	/**
	 * oauth 客户端信息
	 */
	String CLIENT_DETAILS_KEY = "pig_oauth:client:details";

	/**
	 * 参数缓存
	 */
	String PARAMS_DETAILS = "params_details";

}
