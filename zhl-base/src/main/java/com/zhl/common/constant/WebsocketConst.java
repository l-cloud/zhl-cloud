package com.zhl.common.constant;

/**
 * @Description: Websocket常量类
 * @author: taoyan
 * @date: 2020年03月23日
 */
public class WebsocketConst {


    /**
     * 消息json key:cmd
     */
     String MSG_CMD = "cmd";

    /**
     * 消息json key:msgId
     */
     String MSG_ID = "msgId";

    /**
     * 消息json key:msgTxt
     */
     String MSG_TXT = "msgTxt";

    /**
     * 消息json key:userId
     */
     String MSG_USER_ID = "userId";

    /**
     * 消息类型 heartcheck
     */
     String CMD_CHECK = "heartcheck";

    /**
     * 消息类型 user 用户消息
     */
     String CMD_USER = "user";

    /**
     * 消息类型 topic 系统通知
     */
     String CMD_TOPIC = "topic";

    /**
     * 消息类型 email
     */
     String CMD_EMAIL = "email";

    /**
     * 消息类型 meetingsign 会议签到
     */
     String CMD_SIGN = "sign";

}
