package com.zhl.common.enums;

/**
 * 异常返回码枚举接口
 * @author zhl
 * @createTime 2017-12-25 13:46
 */
public interface BaseResultCode {
    /**
     * 异常编码
     *
     * @return
     */
    int getCode();

    /**
     * 异常消息
     *
     * @return
     */
    String getMsg();
}
