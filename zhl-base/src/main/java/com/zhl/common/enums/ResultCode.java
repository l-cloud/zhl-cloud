package com.zhl.common.enums;

import lombok.Getter;

/**
 * @author RudeCrab
 * @description 响应码枚举
 */
@Getter
public enum ResultCode implements BaseResultCode {
    SUCCESS(1000, "操作成功"),
    FAILED(1001, "响应失败"),
    VALIDATE_FAILED(1002, "参数校验失败"),
    ERROR(5000, "未知错误"),
    OK(200, "成功."),
    INTERNAL_SERVER_ERROR(500, "系统异常."),
    BAD_REQUEST(400,"参数不合法."),
    UNAUTHORIZED(401,"无权限."),
    FORBIDDEN(403,"拒绝访问."),
    NOT_FOUND(404, "未找到."),
    METHOD_NOT_ALLOWED(405, "方法不支持."),
    ILLEGAL_ACCESS(406, "非法获取."),
    REPEAT_COMMIT(111, "重复提交."),
    DATA_NOT_FOUND(6000, "数据不存在或已被删除."),
    ACCOUNT_NOT_FOUND(6001, "账号未注册."),
    ACCOUNT_BEEN_USED(6002, "此账号已被注册."),
    ACCOUNT_PWD_ERROR(6003, "账号或密码错误."),
    TENANT_NOT_FOUND(6004, "承租人信息不存在或已被停用."),
    FILE_TOO_LARGE(6014, "文件大小超出10MB限制, 请压缩或降低文件质量."),
    FIELD_TOO_LARGE(6013, "字段太长,超出数据库字段的长度."),

    TEMPLATE_PARSE_ERROR(6015, "模板解析错误."),
    TEMPLATE_DATA_NULL(6016, "模板参数为空."),
    EXPORT_EXCEL_ERROR(6017, "导出excel失败"),
    EXPORT_PDF_ERROR(6015, "导出pdf失败"),
    EXCEL_NO_DATA(6018, "表格无数据"),
    EXCEL_ANALYZE_FAIL(6019, "解析excel失败"),
    EXCEL_TYPE_ERROR(6020, "文件类型错误"),
    UPLOAD_EXCEL_TEMPLATE_ERROR(6021, "上传模板错误"),



    SIGN_ERROR(7001, "签名验证失败."),
    APP_KEY_ERROR(7002, "appKey错误."),
    SIGN_EXPIRED(7003, "请求已经失效."),

    FTP_REFUSE_CONNECT(5001,"FTP服务器拒绝连接"),
    FILE_PATH_NOT_EXIST(5004,"文件路径不存在:{}"),
    FTP_LOGIN_FAIL(5002,"ftp登录失败"),
    FILE_CREATE_FAIL(5003,"目录创建失败:{}"),
    FILE_UPLOAD_FAIL(5005,"文件上传失败:{}"),
    FILE_DOWNLOAD_FAIL(5005,"文件下载失败:{}"),
    FILE_DELETE_FAIL(5005,"文件删除失败:{}"),

    BASE_VALID_PARAM(-9, "统一验证参数异常"),
    JSON_PARSE_ERROR(-13, "JSON解析异常"),
    TOO_MUCH_DATA_ERROR(2002, "批量新增数据过多"),
    SERVICE_MAPPER_ERROR(-11, "Mapper类转换异常"),

    //系统相关 start
    SYSTEM_BUSY(-1, "系统繁忙~请稍后再试~"),
    SYSTEM_TIMEOUT(-2, "系统维护中~请稍后再试~"),
    PARAM_EX(-3, "参数类型解析异常"),
    SQL_EX(-4, "运行SQL出现异常"),
    NULL_POINT_EX(-5, "空指针异常"),
    ILLEGALA_ARGUMENT_EX(-6, "无效参数异常"),
    MEDIA_TYPE_EX(-7, "请求类型异常"),
    LOAD_RESOURCES_ERROR(-8, "加载资源出错"),
    OPERATION_EX(-10, "操作异常"),
    CAPTCHA_ERROR(-12, "验证码校验失败"),
    REQUIRED_FILE_PARAM_EX(1001, "请求中必须至少包含一个有效文件"),

    JWT_BASIC_INVALID(40000, "无效的基本身份验证令牌"),
    JWT_TOKEN_EXPIRED(40001, "会话超时，请重新登录"),
    JWT_SIGNATURE(40002, "不合法的token，请认真比对 token 的签名"),
    JWT_ILLEGAL_ARGUMENT(40003, "缺少token参数"),
    JWT_GEN_TOKEN_FAIL(40004, "生成token失败"),
    JWT_PARSER_TOKEN_FAIL(40005, "解析用户身份错误，请重新登录！"),
    JWT_USER_INVALID(40006, "用户名或密码错误"),
    JWT_USER_ENABLED(40007, "用户已经被禁用！"),
    JWT_OFFLINE(40008, "您已在另一个设备登录！");

    private int code;
    private String msg;

    private ResultCode(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return this.code;
    }

    public String getMsg() {
        return this.msg;
    }

    public ResultCode build(String msg, Object... param) {
        this.msg = String.format(msg, param);
        return this;
    }

    public ResultCode param(Object... param) {
        this.msg = String.format(this.msg, param);
        return this;
    }
}
