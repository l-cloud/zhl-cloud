package com.zhl.gateway.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.ratelimit.KeyResolver;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * @author 凌晨
 * @Title: CustomHostAddrKeyResolver
 * @Description 自定义主机进行限流
 * @date： 2020/10/16 21:45
 * @version： V1.0
 */
@Slf4j
public class CustomHostAddrKeyResolver implements KeyResolver {

    /**
     * bean的名称
     */
    public static final String KEY_RESOLVER_HOST = "customHostAddrKeyResolver";

    /**
     * @Description: 依据主机ip进行限流过滤
     * @Param: [exchange]
     * @return: reactor.core.publisher.Mono<java.lang.String>
     * @Author: peikunkun
     * @Date: 2019/8/23 0023 下午 4:57
     */
    @Override
    public Mono<String> resolve(ServerWebExchange exchange) {
        log.info(this.getClass().getSimpleName() + "resolve  is init");
        return Mono.just(exchange.getRequest().getRemoteAddress().getAddress().getHostAddress());
    }
}
