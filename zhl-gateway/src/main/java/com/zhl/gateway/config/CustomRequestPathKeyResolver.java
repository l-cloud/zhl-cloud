package com.zhl.gateway.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.ratelimit.KeyResolver;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * @author 凌晨
 * @Title: CustomRequestPathKeyResolver
 * @Description 自定义主机进行限流
 * @date： 2020/10/16 21:48
 * @version： V1.0
 */
@Slf4j
public class CustomRequestPathKeyResolver implements KeyResolver {

    /**
     * bean的名称
     */
    public static final String KEY_RESOLVER_REQUEST_URL = "customRequestPathKeyResolver";


    /**
     * @Description: 把根据路径方式进行解析
     * @Param: [exchange]
     * @return: reactor.core.publisher.Mono<java.lang.String>
     * @Author: peikunkun
     * @Date: 2019/8/23 0023 下午 4:52
     */
    @Override
    public Mono<String> resolve(ServerWebExchange exchange) {
        log.info(this.getClass().getSimpleName() + "resolve  is init");
        return Mono.just(exchange.getRequest().getPath().value());
    }

}
