//package com.zhl.gateway.config;
//
//import com.zhl.gateway.filter.CustomGlobalFilter;
//import com.zhl.gateway.filter.RateCheckGatewayFilterFactory;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.cloud.gateway.filter.ratelimit.KeyResolver;
//import org.springframework.cloud.gateway.filter.ratelimit.RateLimiter;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import reactor.core.publisher.Mono;
//
//import com.zhl.gateway.config.CustomHostAddrKeyResolver;
//import com.zhl.gateway.config.CustomRequestPathKeyResolver;
//
//import static com.zhl.gateway.config.CustomHostAddrKeyResolver.KEY_RESOLVER_HOST;
//import static com.zhl.gateway.config.CustomRequestPathKeyResolver.KEY_RESOLVER_REQUEST_URL;
//
///**
// * @author 凌晨
// * @Title: GatewayFilterConfig
// * @Description 过滤器配置类
// * @date： 2020/10/16 21:50
// * @version： V1.0
// */
//@Slf4j
//@Configuration
//public class GatewayFilterConfig {
//
//    @Bean
//    public RateCheckGatewayFilterFactory rateCheckGatewayFilterFactory(RateLimiter defaultRateLimiter,
//                                                                       @Qualifier("apiKeyResolver") KeyResolver defaultKeyResolver) {
//        return new RateCheckGatewayFilterFactory(defaultRateLimiter, defaultKeyResolver);
//    }
//
//
//    /**
//     * @Description: 全局过滤配置器
//     * @Param: []
//     * @return: com.pkk.cloud.support.gataway.filter.CustomGlobalFilter
//     * @Author: peikunkun
//     * @Date: 2019/8/23 0023 上午 11:28
//     */
//    @Bean
//    public CustomGlobalFilter customGlobalFilter() {
//        return new CustomGlobalFilter();
//    }
//
//
//    /**
//     * @Description: 自定义请求路径
//     * @Param: []
//     * @return: com.pkk.cloud.support.gataway.config.custom.CustomRequestPathKeyResolver
//     * @Author: peikunkun
//     * @Date: 2019/8/23 0023 下午 4:51
//     */
//    @Bean(KEY_RESOLVER_REQUEST_URL)
//    public CustomRequestPathKeyResolver customRequestPathKeyResolver() {
//        log.info(this.getClass().getSimpleName() + " is init");
//        return new CustomRequestPathKeyResolver();
//    }
//
//
//    /**
//     * @Description:配置依据主机的方式
//     * @Param: []
//     * @return: com.pkk.cloud.support.gataway.config.custom.CustomHostAddrKeyResolver
//     * @Author: peikunkun
//     * @Date: 2019/8/23 0023 下午 4:59
//     */
//    @Bean(KEY_RESOLVER_HOST)
//    public CustomHostAddrKeyResolver hostAddrKeyResolver() {
//        log.info(this.getClass().getSimpleName() + " is init");
//        return new CustomHostAddrKeyResolver();
//    }
//
//
//    /**
//     * 接口限流操作
//     *
//     * @return
//     */
//    @Bean(name = "apiKeyResolver")
//    public KeyResolver apiKeyResolver() {
//        log.info("根据路径初始化配置：" + this.getClass().getSimpleName());
//        //根据api接口来限流
//        return exchange -> Mono.just(exchange.getRequest().getPath().value());
//    }
//}
