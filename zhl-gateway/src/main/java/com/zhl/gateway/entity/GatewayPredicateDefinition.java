package com.zhl.gateway.entity;

import lombok.Data;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author 凌晨
 * @Title: GatewayPredicateDefinition
 * @Description 路由断言模型
 * @date： 2020/10/18 10:59
 * @version： V1.0
 */
@Data
public class GatewayPredicateDefinition {
    //断言对应的Name
    private String name;
    //配置的断言规则
    private Map<String, String> args = new LinkedHashMap<>();
}
