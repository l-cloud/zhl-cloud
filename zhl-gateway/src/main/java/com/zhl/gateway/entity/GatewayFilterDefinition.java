package com.zhl.gateway.entity;

import lombok.Data;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author 凌晨
 * @Title: GatewayFilterDefinition
 * @Description TODO
 * @date： 2020/10/18 10:58
 * @version： V1.0
 */
@Data
public class GatewayFilterDefinition {
    //Filter Name
    private String name;
    //对应的路由规则
    private Map<String, String> args = new LinkedHashMap<>();
}
