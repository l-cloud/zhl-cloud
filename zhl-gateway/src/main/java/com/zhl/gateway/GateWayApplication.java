package com.zhl.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author 凌晨
 * @Title: GateWayApplication
 * @Description TODO
 * @date： 2020/10/16 20:08
 * @version： V1.0
 */
@SpringBootApplication(scanBasePackages = "com.zhl")
@EnableDiscoveryClient
//actuator/gateway/routes，展示路由列表
//actuator/gateway/globalfilters 展示所有的全局过滤器
//actuator/gateway/routefilters 展示所有的过滤器工厂（GatewayFilter factories）
//actuator/gateway/refresh 清空路由缓存 POST【无消息体】
//actuator/gateway/routes/{id} 展示指定id的路由的信息
//actuator/gateway/routes/{id} POST【消息体如下】新增一个路由
//
//
public class GateWayApplication {
    public static void main(String[] args) {
        SpringApplication.run(GateWayApplication.class);
    }
}
