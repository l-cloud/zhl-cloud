//package com.zhl.gateway.filter;
//
//import org.springframework.context.annotation.Bean;
//import org.springframework.http.HttpHeaders;
//import org.springframework.web.cors.CorsConfiguration;
//import org.springframework.web.cors.reactive.CorsConfigurationSource;
//import org.springframework.web.cors.reactive.CorsWebFilter;
//import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;
//import org.springframework.web.reactive.config.CorsRegistry;
//import org.springframework.web.reactive.config.WebFluxConfigurer;
//
///**
// * @author 凌晨
// * @Title: CustomWebFluxConfig
// * @Description 跨域配置
// * @date： 2020/10/18 11:36
// * @version： V1.0
// */
////webflux跨域配置有两种方式：一种是复写WebFluxConfigurer#addCorsMappings，
////        另一种是配置自定义的CorsWebFilter，两种方式都有一定局限，
////        CorsRegistry的方式无法实现RouteFunctions配置的路由跨域，而CorsWebFilter的方式只是单纯的拦截请求，
////        其他框架层的代码无法读取到跨域的配置，比如说RequestMappingHandlerMapping#getHandler时就无法读取到跨域配置，可以考虑两者都配置。
////    webflux已经没有了Interceptor的概念，但是可以通过WebFilter的方式实现
//public class CustomWebFluxConfig implements WebFluxConfigurer {
//    /**
//     * 全局跨域配置，根据各自需求定义
//     * @param registry
//     */
//    @Override
//    public void addCorsMappings(CorsRegistry registry) {
//        registry.addMapping("/**")
//                .allowCredentials(true)
//                .allowedOrigins("*")
//                .allowedHeaders("*")
//                .allowedMethods("*")
//                .exposedHeaders(HttpHeaders.SET_COOKIE);
//    }
//
//    /**
//     * 也可以继承CorsWebFilter使用@Component注解，效果是一样的
//     * @return
//     */
//    @Bean
//    CorsWebFilter corsWebFilter(){
//        CorsConfiguration corsConfiguration = new CorsConfiguration();
//        corsConfiguration.setAllowCredentials(true);
//        corsConfiguration.addAllowedHeader("*");
//        corsConfiguration.addAllowedMethod("*");
//        corsConfiguration.addAllowedOrigin("*");
//        corsConfiguration.addExposedHeader(HttpHeaders.SET_COOKIE);
//        CorsConfigurationSource corsConfigurationSource = new UrlBasedCorsConfigurationSource();
//        ((UrlBasedCorsConfigurationSource) corsConfigurationSource).registerCorsConfiguration("/**",corsConfiguration);
//        return new CorsWebFilter(corsConfigurationSource);
//    }
//}
