package com.zhl.gateway.util;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.zhl.gateway.entity.GatewayRouteDefinition;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;

/**
 * @author 凌晨
 * @Title: JsonUtil
 * @Description TODO
 * @date： 2020/10/18 14:39
 * @version： V1.0
 */
@Slf4j
public class JsonUtil {
    private static ObjectMapper objectMapper = new ObjectMapper();
//    static {
//        //将对象的所有字段全部列入
//        objectMapper.setSerializationInclusion(JsonSerialize.Inclusion.ALWAYS);
//
//        //取消默认转换timestamps形式,false使用日期格式转换，true不使用日期转换，结果是时间的数值157113793535
//        objectMapper.configure(SerializationConfig.Feature.WRITE_DATE_KEYS_AS_TIMESTAMPS,false); //默认值true
//
//        //忽略空Bean转json的错误
//        objectMapper.configure(SerializationConfig.Feature.FAIL_ON_EMPTY_BEANS,false);
//
//        //所有的日期格式统一样式： yyyy-MM-dd HH:mm:ss
//        objectMapper.setDateFormat(new SimpleDateFormat(DateTimeUtil.STANDARD_FORMAT));
//
//        //忽略 在json字符串中存在，但是在对象中不存在对应属性的情况，防止错误。
//        // 例如json数据中多出字段，而对象中没有此字段。如果设置true，抛出异常，因为字段不对应；false则忽略多出的字段，默认值为null，将其他字段反序列化成功
//        objectMapper.configure(DeserializationConfig.Feature.FAIL_ON_NULL_FOR_PRIMITIVES,false);
//
//    }

    //将单个对象转换成json格式的字符串（没有格式化后的json）
    public static <T> String obj2String(T obj){
        if (obj == null){
            return null;
        }
        try {
            return obj instanceof String ? (String) obj:objectMapper.writeValueAsString(obj);
        } catch (IOException e) {
            log.warn("Parse object to String error", e);
            return null;
        }
    }

    //将单个对象转换成json格式的字符串（格式化后的json）
    public static <T> String obj2StringPretty(T obj){
        if (obj == null){
            return null;
        }
        try {
            return obj instanceof String ? (String) obj:objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(obj);
        } catch (IOException e) {
            log.warn("Parse object to String error", e);
            return null;
        }
    }
    //将json形式的字符串数据转换成单个对象
    public static <T> T string2Obj(String str, Class<T> clazz){
        if (StringUtils.isEmpty(str) || clazz == null){
            return null;
        }
        try {
            return clazz.equals(String.class) ? (T) str : objectMapper.readValue(str,clazz);
        } catch (IOException e) {
            log.warn("Parse object to Object error", e);
            return null;
        }
    }

    //将json形式的字符串数据转换成多个对象
    public static <T> T string2Obj(String str, TypeReference<T> typeReference){
        if (StringUtils.isEmpty(str) || typeReference == null){
            return null;
        }
        try {
            return typeReference.getType().equals(String.class) ? (T) str : (T) objectMapper.readValue(str, typeReference);
        } catch (IOException e) {
            log.warn("Parse object to Object error", e);
            return null;
        }
    }

    //将json形式的字符串数据转换成多个对象
    public static <T> T string2Obj(String str, Class<T> collectionClass, Class<?>... elementClasses){
        JavaType javaType = objectMapper.getTypeFactory().constructParametricType(collectionClass,elementClasses);
        try {
            return objectMapper.readValue(str, javaType);
        } catch (IOException e) {
            log.warn("Parse object to Object error", e);
            return null;
        }
    }

    //测试
    public static void main(String[] args) {

        String str = "{\n" +
                "    \"id\": \"zhl-task\",\n" +
                "    \"uri\": \"lb://zhl-task\",\n" +
                "    \"filters\": [{\n" +
                "      \"name\": \"TokenCheck\",\n" +
                "      \"args\": {\n" +
                "        \"enable\": \"true\",\n" +
                "        \"authUrl\": \"www\",\n" +
                "        \"ignoreGlobalFilter\": \"false\"\n" +
                "      }\n" +
                "    },\n" +
                "      {\n" +
                "        \"name\": \"StripPrefix\",\n" +
                "        \"args\": {\n" +
                "          \"parts\": \"2\"\n" +
                "        }\n" +
                "      }\n" +
                "    ],\n" +
                "    \"predicates\": [{\n" +
                "      \"args\": {\n" +
                "        \"pattern\": \"/rest/project/**\"\n" +
                "      },\n" +
                "      \"name\": \"Path\"\n" +
                "    }]\n" +
                "  }";
        GatewayRouteDefinition gatewayRouteDefinition = string2Obj(str, GatewayRouteDefinition.class);
        System.out.println(gatewayRouteDefinition);
//        User user1 = new User();
//        user1.setId(1);
//        user1.setUsername("wangjun1");
//        user1.setCreateTime(new Date());
//        String user1JsonPretty = JsonUtil.obj2StringPretty(user1);
//        log.info("user1JsonPretty:{}", user1JsonPretty);
//        User user2 = new User();
//        user2.setId(2);
//        user2.setUsername("wangjun2");
//
//        String user1Json = JsonUtil.obj2String(user1);
//
//        String user1JsonPretty = JsonUtil.obj2StringPretty(user1);
//
//        log.info("user1Json:{}", user1Json);
//
//        log.info("user1JsonPretty:{}", user1JsonPretty);
//
//
//        User user = JsonUtil.string2Obj(user1Json, User.class);
//        System.out.println(user.getUsername());
//
//        List<User> userList = Lists.newArrayList();
//        userList.add(user1);
//        userList.add(user2);
//
//        String userListStr = JsonUtil.obj2StringPretty(userList);
//
//        log.info("==================");
//        log.info(userListStr);
//
//        List<User> userListObj1 = JsonUtil.string2Obj(userListStr, new TypeReference<List<User>>() {
//        });
//        System.out.println(userListObj1);
//
//        List<User> userListObj2 = JsonUtil.string2Obj(userListStr,List.class,User.class);
//        System.out.println(userListObj2);
//        System.out.println("end");
    }
}
