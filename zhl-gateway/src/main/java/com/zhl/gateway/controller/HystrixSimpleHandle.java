package com.zhl.gateway.controller;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 凌晨
 * @Title: HystrixSimpleHandle
 * @Description  熔断器的简单处理
 * @date： 2020/10/16 21:52
 * @version： V1.0
 */
@Slf4j
@RestController
public class HystrixSimpleHandle {

    /**
     *  args:
     *   name: authHystrixCommand
     *   fallbackUri: forward:/hystrixTimeout
     *   name表示HystrixCommand代码的名称，fallbackUri表示触发断路由后的跳转请求url
     */

    /**
     * @Description: 熔断器的处理
     * @Param: []
     * @return: java.lang.String
     * @Author: peikunkun
     * @Date: 2019/8/11 0011 下午 5:38
     */
    @HystrixCommand(commandKey = "authHystrixCommand")
    public String authHystrixCommand() {
        return "进入熔断器的处理类";
    }

    /**
     * 熔断器出发类
     *
     * @return
     */
//    @RequestMapping("/hystrixTimeout")
//    public CommonResponse<Object> hystrixTimeout() {
//        log.debug("cloud-support-gataway触发了断路由");
//        return ResponseUtil.error(HttpStatus.SERVICE_UNAVAILABLE.value(), HttpStatus.SERVICE_UNAVAILABLE.getReasonPhrase());
//    }
}
