//package com.zhl.file.util;
//
//import com.aliyun.oss.OSS;
//import com.aliyun.oss.model.*;
//import lombok.extern.slf4j.Slf4j;
//import org.apache.commons.lang.StringUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.stereotype.Component;
//import org.springframework.web.multipart.MultipartFile;
//
//import java.io.IOException;
//import java.io.InputStream;
//import java.net.MalformedURLException;
//import java.net.URL;
//import java.util.*;
//
///**
// * @author 凌晨
// * @Title: OssUploadUtil
// * @Description
// * @date： 2020/1/19 11:35
// * @version： V1.0
// */
//@Component
//@Slf4j
//public class OssUploadUtil {
//
//    @Autowired
//    private OSS ossClient;
//
//    @Value("${alibaba.oss.bucket}")
//    private String bucketName;
//
//    /**
//     *
//     * @param multipartFile 上传的文件
//     * @param path 指定oss上传路径
//     * @return
//     */
//    public String uploadFileByPath(MultipartFile multipartFile, String path) {
//        try {
//            MultipartFile file = multipartFile;
//            path = path+getDateUrl();
//            String key = path+UUID.randomUUID().toString()+ file.getOriginalFilename();
//            System.out.println("文件名称-----------------------------"+file.getOriginalFilename());
//            ossClient.putObject(bucketName, key, file.getInputStream());
//            OSSObject object = ossClient.getObject(bucketName, key);
////            ossClient.shutdown();
//            String uri = object.getResponse().getUri();
//            if(uri.contains("http")){
//                uri = uri.replace("http","https");
//            }
//            return uri;
//        } catch (MalformedURLException e) {
//            log.error(e.toString());
//        } catch (IOException e) {
//            log.error(e.toString());
//        }
//        return null;
//
//    }
//    /**
//     * 上传网络流文件
//     * @param filePath 网络流文件地址
//     * @param newFilPath 新的文件地址
//     * @return
//     */
//    public String netWorkUpload(String filePath,String newFilPath){
//        if(StringUtils.isBlank(filePath)){
//            return "";
//        }
//        log.info("*****开始上传网络流文件*****");
//        String fileName = filePath.substring(filePath.lastIndexOf("/")+1,filePath.length());
//        newFilPath = newFilPath + getDateUrl()+ fileName;//数据库配置的路径+年月日+文件名
//        InputStream inputStream = null;
//        try {
//            inputStream = new URL(filePath).openStream();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        log.info("网络文件新的上传地址为===="+newFilPath);
//        PutObjectResult putObjectResult = ossClient.putObject(bucketName, newFilPath, inputStream);
//        String url = getUrl(newFilPath,ossClient);
////        ossClient.shutdown();
//        try {
//            inputStream.close();
//        } catch (IOException e1) {
//            e1.printStackTrace();
//        }
//        return url;
//    }
//
//    /**
//     * 上传网络流文件
//     * @param filePaths 网络流文件地址集合
//     * @param newFilPath 新的文件地址
//     * @return
//     */
////    public String netWorkUpload(List<String> filePaths,String newFilPath){
////        for (String filePath : filePaths) {
////            log.info("*****开始上传网络流文件*****");
////            String fileName = filePath.substring(filePath.lastIndexOf("/") + 1, filePath.length());
////            newFilPath = newFilPath + getDateUrl() + fileName;//数据库配置的路径+年月日+文件名
////            InputStream inputStream = null;
////            try {
////                inputStream = new URL(filePath).openStream();
////            } catch (IOException e) {
////                e.printStackTrace();
////            }
////            log.info("网络文件新的上传地址为====" + newFilPath);
////            PutObjectResult putObjectResult = ossClient.putObject(bucketName, newFilPath, inputStream);
////            String url = getUrl(newFilPath, ossClient);
//////        ossClient.shutdown();
////            try {
////                inputStream.close();
////            } catch (IOException e1) {
////                e1.printStackTrace();
////            }
////        }
////
////        return url;
////    }
//
//
//    /**
//     * 列举文件下所有的文件url信息
//     */
//    public List<String> listFile(String fileHost) {
//        // 创建OSSClient实例。
//        // 构造ListObjectsRequest请求
//        ListObjectsRequest listObjectsRequest = new ListObjectsRequest(bucketName);
//
//        // 设置prefix参数来获取fun目录下的所有文件。
//        listObjectsRequest.setPrefix(fileHost + "/");
//        // 列出文件。
//        ObjectListing listing = ossClient.listObjects(listObjectsRequest);
//        // 遍历所有文件。
//        List<String> list = new ArrayList<>();
//        List<OSSObjectSummary> sums = listing.getObjectSummaries();
//        sums.forEach(x->{
//            System.out.println("\t" + x.getKey());
//            list.add(x.getKey());
//        });
//        // 关闭OSSClient。
////        ossClient.shutdown();
//        return list;
//    }
//
//    /**
//     * 删除文件
//     * @param filePath
//     * @return
//     */
//    public boolean deleteFile(String filePath) {
//        boolean exist = ossClient.doesObjectExist(bucketName, filePath);
//        if (!exist) {
//            log.error("文件不存在,filePath={}", filePath);
//            return false;
//        }
//        log.info("删除文件,filePath={}", filePath);
//        ossClient.deleteObject(bucketName, filePath);
////        ossClient.shutdown();
//        return true;
//
//    }
//
//    /**
//     * 批量删除文件
//     * @param filePaths
//     * @return
//     */
//    public boolean batchDeleteFile(List<String> filePaths) {
//        DeleteObjectsResult deleteObjectsResult = ossClient.deleteObjects(new DeleteObjectsRequest(bucketName).withKeys(filePaths));
//        List<String> deletedObjects = deleteObjectsResult.getDeletedObjects();
//        for (String filePath: deletedObjects) {
//            log.info("删除失败，文件路径==="+filePath);
//        }
//        // 关闭OSSClient。
////        ossClient.shutdown();
//        return true;
//    }
//
//    /**
//     * 生成年月日目录
//     * @return
//     */
//    public static String getDateUrl(){
//        Calendar calendar = Calendar.getInstance();
//        /**按年月日来分*/
//        int year = calendar.get(Calendar.YEAR);//得到年
//        int month = calendar.get(Calendar.MONTH)+1;//得到月，因为从0开始的，所以要加1
//        int day = calendar.get(Calendar.DAY_OF_MONTH);//得到天
//
//        /**保存到数据库中的图片地址*/
//        String dateUrl = year + "/" + month + "/" + day + "/";  //新的图片url
//        return dateUrl;
//    }
//
//    /**
//     * 获得url链接
//     *
//     * @param key
//     * @return
//     */
//    public String getUrl(String key,OSS ossClient) {
//        // 设置URL过期时间为10年  3600l* 1000*24*365*10
//        Date expiration = new Date(new Date().getTime() + 3600l * 1000 * 24 * 365 * 10);
//        // 生成URL
//        URL url = ossClient.generatePresignedUrl(bucketName, key, expiration);
//        if (url != null) {
//            String fullUrl = url.toString();
//            fullUrl = fullUrl.substring(0,fullUrl.lastIndexOf("?Expires"));
//            return fullUrl;
//        }
//        return null;
//    }
//
//    /**
//     * 上传某个Object
//     * @param inputStream
//     * @return
//     */
//    public String putObject(InputStream inputStream,String name, String address) {
//        PutObjectResult result = null;
//        String url = null;
//        try {
//            // 创建上传Object的Metadata
//            ObjectMetadata meta = new ObjectMetadata();
//            // 设置文件类型
//            meta.setContentType("image/jpeg");
//            // 上传Object.
//            String key = address+name+".jpeg";
//            result = ossClient.putObject(bucketName, key, inputStream, meta);
//            url = getUrl(key, ossClient);
//            log.info("上传文件到oss返回result={}",result);
//        } catch (Exception e) {
//            log.info(e.getMessage());
//            return null;
//        }
//        return url;
//    }
//
//
//
//}
