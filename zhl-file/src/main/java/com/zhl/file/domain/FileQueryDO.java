package com.zhl.file.domain;


import com.zhl.file.entity.File;
import lombok.Data;

/**
 * 文件查询 DO
 *
 * @author zhl
 * @date 2019/05/07
 */
@Data
public class FileQueryDO extends File {
    private File parent;

}
