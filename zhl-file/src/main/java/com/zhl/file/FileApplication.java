package com.zhl.file;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.File;

/**
 * @author zhl
 * @Title: FileApplication
 * @Description TODO
 * @date： 2020/10/15 9:35
 * @version： V1.0
 */
@SpringBootApplication(scanBasePackages = "com.zhl")
public class FileApplication {

    public static void main(String[] args) {
        SpringApplication.run(FileApplication.class);
    }


}
