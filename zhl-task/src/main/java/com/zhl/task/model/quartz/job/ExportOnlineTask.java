//package com.zhl.task.model.quartz.job;
//
//import cn.afterturn.easypoi.excel.ExcelExportUtil;
//import cn.afterturn.easypoi.excel.entity.ExportParams;
//import cn.hutool.core.date.DateUtil;
//import com.zhl.model.email.service.MailService;
//import com.zhl.model.excel.entity.BiddingProjectVo;
//import com.zhl.model.excel.service.ExcelStyleUtil;
//import com.zhl.model.excel.service.ExportOnlineService;
//import lombok.extern.slf4j.Slf4j;
//import org.apache.poi.ss.usermodel.Workbook;
//import org.springframework.beans.factory.InitializingBean;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.scheduling.annotation.EnableScheduling;
//import org.springframework.scheduling.annotation.Scheduled;
//
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.util.*;
//
///**
// * @author zhl
// * @Title: ExportOnlineTask
// * @Description TODO
// * @date： 2020/9/17 9:33
// * @version： V1.0
// */
//@EnableScheduling
//@Configuration
//@Slf4j
//public class ExportOnlineTask implements InitializingBean {
//
//    @Autowired
//    private MailService mailService;
//
//    @Autowired
//    private ExportOnlineService exportOnlineService;
//
//    @Value("${mail.receiveEmail.addr}")
//    private String receiveEmail;
//
//    @Value("${report.localPath.temp}")
//    private String filePath;
//
//    @Scheduled(cron = "0 */10 * * * ?")
//    public void sendExcel(){
//        log.info("************************统计国际全流程项目定时任务开始************************");
//        String lxsj = "20190000000000";
//        String dljgbh = "001";
//        long startTime = System.currentTimeMillis();
//        String currentDate = DateUtil.format(new Date(), "yyyyMMddHHmmss");
//        Map<String, Object> parameterSource = new HashMap<>();
//        parameterSource.put("lxsj", lxsj);
//        parameterSource.put("dljgbh", dljgbh);
//        parameterSource.put("lybz", Arrays.asList(1,2));
//        List<BiddingProjectVo> biddingProjectVoList = exportOnlineService.queryProjectInfo(parameterSource);
//        ExportParams exportParams = new ExportParams("国际报表全流程项目统计表","全流程项目统计表");
//        exportParams.setStyle(ExcelStyleUtil.class);
//        Workbook workbook = ExcelExportUtil.exportBigExcel(exportParams,
//                BiddingProjectVo.class, biddingProjectVoList);
//        FileOutputStream fos = null;
//        String fileName = filePath + "exportOnline_"+currentDate+".xlsx";
//        try {
//            fos = new FileOutputStream(fileName);
//            workbook.write(fos);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }finally {
//            try {
//                fos.close();
//                //重复导出失败，每次关闭这个即可Stream closed
//                ExcelExportUtil.closeExportBigExcel();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//        log.info("导出excel耗时==="+(System.currentTimeMillis()-startTime)+"毫秒");
//        log.info("发送到邮箱==============="+receiveEmail);
//        mailService.sendAttachmentsMail(receiveEmail, "主题：国际全流程项目统计", "国际全流程项目统计，请查收！", fileName);
//        log.info("************************统计国际全流程项目定时任务结束************************");
//    }
//
//    /**
//     * 启动服务时执行一遍工作
//     */
//    @Override
//    public void afterPropertiesSet() {
//        try {
//            this.sendExcel();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//}
