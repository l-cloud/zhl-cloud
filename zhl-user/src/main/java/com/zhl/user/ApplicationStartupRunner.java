package com.zhl.user;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * @author zhl
 * @Title: ApplicationStartupRunner
 * @Description TODO
 * @date： 2020/10/21 17:13
 * @version： V1.0
 */
@Component
public class ApplicationStartupRunner implements ApplicationRunner {
    @Override
    public void run(ApplicationArguments args) throws Exception {
        System.out.println("applicationRunner args: ");
        System.out.println("applicationRunner args: ");
        System.out.println(args.getOptionValues("name"));
        System.out.println(args.getOptionValues("password"));
    }
}
