package com.zhl.user.api;


import com.zhl.user.api.factory.MonitorServiceFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "zhl-monitor",fallbackFactory = MonitorServiceFallbackFactory.class)
public interface MonitorService {

    @GetMapping("/test")
    String test(@RequestParam(value = "param") String param);

}
