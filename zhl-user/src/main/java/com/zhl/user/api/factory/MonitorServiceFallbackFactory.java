package com.zhl.user.api.factory;

import com.zhl.user.api.MonitorService;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author 凌晨
 * @Title: ServiceFallbackFactory
 * @Description TODO
 * @date： 2021/6/24 20:39
 * @version： V1.0
 */
@Slf4j
@Component
public class MonitorServiceFallbackFactory implements FallbackFactory<MonitorService> {
    @Override
    public MonitorService create(Throwable throwable) {
        return new MonitorService() {
            @Override
            public String test(String param) {
                return "系统超时熔断";
            }
        };
    }
}
