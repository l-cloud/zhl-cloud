package com.zhl.user;

import com.zhl.util.DingDingMsgSendUtils;
import com.zhl.util.DingMsgPhoneEnum;
import com.zhl.util.DingTokenEnum;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author 凌晨
 * @Title: UserApplication
 * @Description TODO
 * @date： 2020/10/20 23:07
 * @version： V1.0
 */
@SpringBootApplication(scanBasePackages = "com.zhl")
@EnableDiscoveryClient
@EnableFeignClients(basePackages = "com.zhl.user.api")
@EnableCircuitBreaker
public class UserApplication {

    @Value("${spring.profiles.active}")
    private static String profile;

    public static void main(String[] args) {
        DingDingMsgSendUtils.sendDingDingGroupMsg(DingTokenEnum.SEND_SMS_BY_DEVELOPER_TOKEN.getToken(),
                "【系统消息】"+profile+"环境，XXX任务开始执行...", DingMsgPhoneEnum.DEVELOPER_PHONE.getPhone());
        SpringApplication.run(UserApplication.class,args);
    }
}
