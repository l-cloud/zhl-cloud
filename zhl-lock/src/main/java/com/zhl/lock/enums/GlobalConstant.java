package com.zhl.lock.enums;

/**
 * 全局常量枚举
 *
 * @author zhl
 * @date 2020-11-11
 */

public interface GlobalConstant {

    /**
     * Redis地址连接前缀
     */
    String REDIS_CONNECTION_PREFIX = "redis://";

}
