package com.zhl.lock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author 凌晨
 * @Title: RedisLockApplication
 * @Description TODO
 * @date： 2021/1/21 20:11
 * @version： V1.0
 */
@SpringBootApplication
public class RedisLockApplication {
    public static void main(String[] args) {
        SpringApplication.run(RedisLockApplication.class,args);
    }
}
