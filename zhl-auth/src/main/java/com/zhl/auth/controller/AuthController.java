package com.zhl.auth.controller;

import com.zhl.common.vo.R;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zhl
 * @Title: AuthController
 * @Description TODO
 * @date： 2020/10/22 14:38
 * @version： V1.0
 */
@RestController
public class AuthController {

    @GetMapping(value = "/test")
    public R test(){
        return R.success("测试");
    }
}
