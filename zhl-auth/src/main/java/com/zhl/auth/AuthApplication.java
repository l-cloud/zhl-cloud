package com.zhl.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author zhl
 * @Title: AuthApplicarion
 * @Description TODO
 * @date： 2020/10/22 14:36
 * @version： V1.0
 */
@SpringBootApplication(scanBasePackages = "com.zhl")
@EnableDiscoveryClient
public class AuthApplication {
    public static void main(String[] args) {
        SpringApplication.run(AuthApplication.class,args);
    }
}
