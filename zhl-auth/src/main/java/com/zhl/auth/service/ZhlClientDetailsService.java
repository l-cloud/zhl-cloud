/*
 * Copyright (c) 2020 pig4cloud Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.zhl.auth.service;

import com.zhl.common.constant.CacheConstants;
import lombok.SneakyThrows;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.client.JdbcClientDetailsService;

import javax.sql.DataSource;

/**
 * 能够使用内存或者JDBC来实现客户端详情服务（ClientDetailsService）
 * ClientDetailsService负责查找ClientDetails，而ClientDetails有几个重要的属性如下列表
 * clientId：（必须的）用来标识客户的Id
 * secret：（需要值得信任的客户端）客户端安全码，如果有的话
 * scope：用来限制客户端的访问范围，如果为空（默认）的话，那么客户端拥有全部的访问范围。
 * authorizedGrantTypes：此客户端可以使用的授权类型，默认为空。
 * authorities：此客户端可以使用的权限（基于Spring Security authorities）。
 * @author lengleng
 * @date 2019/2/1
 * <p>
 * see JdbcClientDetailsService
 */
public class ZhlClientDetailsService extends JdbcClientDetailsService {

	public ZhlClientDetailsService(DataSource dataSource) {
		super(dataSource);
	}

	/**
	 * 重写原生方法支持redis缓存
	 * @param clientId
	 * @return
	 */
	@Override
	@SneakyThrows
	@Cacheable(value = CacheConstants.CLIENT_DETAILS_KEY, key = "#clientId", unless = "#result == null")
	public ClientDetails loadClientByClientId(String clientId) {
		return super.loadClientByClientId(clientId);
	}

}
