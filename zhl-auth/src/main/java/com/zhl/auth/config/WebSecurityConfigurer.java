/*
 * Copyright (c) 2020 pig4cloud Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.zhl.auth.config;

import com.zhl.auth.handler.FormAuthenticationFailureHandler;
import com.zhl.auth.handler.SsoLogoutSuccessHandler;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.logout.LogoutFilter;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

/**
 *  security web安全配置,spring-cloud-starter-oauth2依赖于security
 *  默认情况下WebSecurityConfigurer执行比ResourceServerConfig先
 * @author lengleng
 * 可以看到ResourceServerConfig 是比SecurityConfig 的优先级低的。
 * 二者的关系：
 * ResourceServerConfig 用于保护oauth相关的endpoints，同时主要作用于用户的登录(form login,Basic auth)
 * SecurityConfig 用于保护oauth要开放的资源，同时主要作用于client端以及token的认证(Bearer auth)
 * 所以我们让SecurityConfig优先于ResourceServerConfig，且在SecurityConfig 不拦截oauth要开放的资源，在ResourceServerConfig 中配置需要token验证的资源，
 * 也就是我们对外提供的接口。所以这里对于所有微服务的接口定义有一个要求，就是全部以/api开头。
 * 如果这里不这样配置的话，在你拿到access_token去请求各个接口时会报 invalid_token的提示。
 * 另外，由于我们自定义认证逻辑，所以需要重写UserDetailService
 * @date 2019/2/1 认证相关配置
 */
@Primary
@Order(90)
@Configuration
public class WebSecurityConfigurer extends WebSecurityConfigurerAdapter {

	@Autowired
	private IgnoreLogoutFilter ignoreLogoutFilter;

	// 安全拦截机制（重要）
	// 安全请求配置,这里配置的是security的部分，这里配置全部通过，安全拦截在资源服务的配置文件中配置，
	// 要不然访问未验证的接口将重定向到登录页面，前后端分离的情况下这样并不友好，无权访问接口返回相关错误信息即可
	@Override
	@SneakyThrows
	protected void configure(HttpSecurity http) {
		http.formLogin()
//				.loginPage("/token/login") // 自定义登录页url,默认为/login
//				.usernameParameter() // 用户名的请求字段 默认为userName
//				.passwordParameter() // 密码的请求字段 默认为password
//				.permitAll()
//				.loginProcessingUrl("/token/form") // 登录请求拦截的url,也就是form表单提交时指定的action
				.failureHandler(authenticationFailureHandler()) // 表单登录失败处理逻辑
				.and()
				.logout()
				.logoutSuccessHandler(logoutSuccessHandler()) // 用来自定义退出成功后的操作 用户退出后要被重定向的url
				.deleteCookies("JSESSIONID").invalidateHttpSession(true) // 默认为true,用户在退出后Http session失效
				.and()
				.authorizeRequests().antMatchers("/token/**", "/actuator/**", "/mobile/**").permitAll() // 指定认证页面可以匿名访问
				.anyRequest().authenticated()// 所有资源必须授权后访问
//				.and().authorizeRequests().anyRequest().permitAll()
				.and()
				.csrf().disable() // 关闭跨站请求防护
				.addFilterAt(ignoreLogoutFilter, LogoutFilter.class);
	}

	@Override
	public void configure(WebSecurity web) {
		web.ignoring().antMatchers("/css/**");
	}

	//AuthenticationManager对象在OAuth2认证服务中要使用，提取放入IOC容器中
	@Bean
	@Override
	@SneakyThrows
	public AuthenticationManager authenticationManagerBean() {
		return super.authenticationManagerBean();
	}

	@Bean
	public AuthenticationFailureHandler authenticationFailureHandler() {
		return new FormAuthenticationFailureHandler();
	}

	/**
	 * 支持SSO 退出
	 * @return LogoutSuccessHandler
	 */
	@Bean
	public LogoutSuccessHandler logoutSuccessHandler() {
		return new SsoLogoutSuccessHandler();
	}

	/**
	 * 密码编码器
	 * 配置密码加密对象（解密时会用到PasswordEncoder的matches判断是否正确）
	 * 用户的password和客户端clientSecret用到，所以存的时候存该bean encode过的密码
	 * https://spring.io/blog/2017/11/01/spring-security-5-0-0-rc1-released#password-storage-updated
	 * Encoded password does not look like BCrypt
	 * @return PasswordEncoder
	 */
	@Bean
	public PasswordEncoder passwordEncoder() {
		return PasswordEncoderFactories.createDelegatingPasswordEncoder();
	}

}
