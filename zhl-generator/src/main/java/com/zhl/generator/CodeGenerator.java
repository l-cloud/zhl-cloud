package com.zhl.generator;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

/**
 * @author 凌晨
 * @Title: CodeGenerator 代码逆向生成工具
 * @Description TODO
 * @date： 2020/9/17 21:46
 * @version： V1.0
 */
public class CodeGenerator {
    public static void main(String[] args) {
        // 模块名
        String moduleName = "emp";

        //1、全局配置
        GlobalConfig gc = new GlobalConfig();
        // 获取当前项目根路径
        String projectPath = System.getProperty("user.dir");
        System.out.println("user.dir=="+projectPath);
        gc.setActiveRecord(true);//开启AR模式
        //生成路径(一般都是生成在此项目的src/main/java下面)
        gc.setOutputDir(projectPath+"/src/main/java");
        //设置作者
        gc.setAuthor("zhouhailong");
        //不打开生产的文件
        gc.setOpen(false);
        //不覆盖之前生成的文件
        gc.setFileOverride(false);
        //生成的service接口名字首字母是否为I，这样设置就没有I
//        如下配置 %s 为占位符
        gc.setServiceName("%sService");
        // 主键策略 自增  注意要和数据库中表实际情况对应
        gc.setIdType(IdType.AUTO);
        //生成resultMap
        gc.setBaseResultMap(true);
        //在xml中生成基础列
        gc.setBaseColumnList(true);
        //时间类型对应策略,默认值：TIME_PACK
        gc.setDateType(DateType.ONLY_DATE);
        gc.setSwagger2(true);//自动开启swagger2的支持

        //2、数据源配置
//        spring 2.0x都是配置的mysql8的驱动，8.0以上的驱动需要配置数据库时区！
        DataSourceConfig dsc = new DataSourceConfig();
//        dsc.setDriverName("com.mysql.cj.jdbc.Driver");
        dsc.setDriverName("com.mysql.jdbc.Driver");
        dsc.setUrl("jdbc:mysql://localhost:3306/mp?useSSL=false&useUnicode=true&characterEncoding=utf-8&serverTimezone=GMT%2B8");
        dsc.setUsername("root");
        dsc.setPassword("123456");
        dsc.setDbType(DbType.MYSQL);

        //3、策略配置
        StrategyConfig strategy = new StrategyConfig();
        //是否大写命名
//        strategy.isCapitalMode()
         strategy.setInclude("emp_person","emp_student");//表名，多个英文逗号分割
        //可以用同配符号:表示生成t_开头的对应库下所有表
        //需要包含的表名，当enableSqlFilter为false时，允许正则表达式（与exclude二选一配置）
//        strategy.setInclude("emp"+"_\\w*");
//        strategy.setExclude();//需要排除的表名，当enableSqlFilter为false时，允许正则表达式
        //数据库表字段映射到实体的命名策略, 未指定按照 naming 执行
//        strategy.setColumnNaming();
        strategy.setNaming(NamingStrategy.underline_to_camel);// 下划线转驼峰
        strategy.setTablePrefix("emp_");//去掉t_这个前缀后生成类名
//        strategy.setFieldPrefix();//字段前缀
//        strategy.setSuperEntityClass();//自定义继承的Entity类全称，带包名
        strategy.setEntityLombokModel(true);//自动生成lombok注解  记住要有lombok依赖和对应的插件哈
//        strategy.setLogicDeleteFieldName("is_deleted");//设置逻辑删除字段 要和数据库中表对应哈
//        strategy.setLikeTable();//自3.3.0起，模糊匹配表名（与notLikeTable二选一配置）
        strategy.setChainModel(true);//【实体】是否为链式模型（默认 false）,3.3.2以下版本默认生成了链式模型，3.3.2以后，默认不生成，如有需要，请开启 chainModel
//        strategy.setEntityBooleanColumnRemoveIsPrefix();//Boolean类型字段是否移除is前缀（默认 false）
        strategy.setEntityTableFieldAnnotationEnable(true);//是否生成实体时，生成字段注解

        // 设置创建时间和更新时间自动填充策略
//        TableFill created_date = new TableFill("created_date", FieldFill.INSERT);
//        TableFill updated_date = new TableFill("updated_date", FieldFill.INSERT_UPDATE);
//        ArrayList<TableFill> tableFills = new ArrayList<>();
//        tableFills.add(created_date);
//        tableFills.add(updated_date);
//        strategy.setTableFillList(tableFills);

        // 乐观锁策略
        strategy.setVersionFieldName("version");//乐观锁属性名称
        strategy.setRestControllerStyle(true);//生成 @RestController 控制器,采用restful 风格的api
        strategy.setControllerMappingHyphenStyle(true); // controller 请求地址采用下划线代替驼峰

        //4、包名策略配置
        PackageConfig pc = new PackageConfig();
        //父包模块名
        pc.setModuleName(moduleName);
        //父包名。如果为空，将下面子包名必须写全部， 否则就只需写子包名
        pc.setParent("com.zhl.model");
        //Controller包名
        pc.setController("controller");
        //Service包名
        pc.setService("service");
//        pc.setServiceImpl()
        //Entity包名
        pc.setEntity("entity");
        //Mapper包名
        pc.setMapper("mapper");
//        pc.setXml()

        //5、整合配置
        AutoGenerator autoGenerator = new AutoGenerator();
        autoGenerator.setGlobalConfig(gc)
                .setDataSource(dsc)
                .setStrategy(strategy)
                .setPackageInfo(pc);
        // 执行
        autoGenerator.execute();
    }
}
